#include <iostream>

// Namespace gives the ability to use same variable name for different values;

namespace first{
    int x = 1;
}

namespace second {
    int x = 2;
}

int main(){
    using std::cout;
    cout << first::x + second::x;
    return 0; 
}