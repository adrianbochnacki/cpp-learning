#include <iostream>
#include <sstream> // For std::ostringstream
#include <string>

// Function declaration
int basicInt();
int characters();
int boolean();
std::string  dates();
std::string strings();

int main(){
    //basicInt();
    //characters();
    //boolean();
    strings();
    return 0;
}

int boolean(){
    bool pass = true;
    std::cout << pass;
    return 0;
}

int characters(){
    char currency = '$';
    std::cout << currency;
    return 0;
}

std::string strings(){
    std::string Name = "Bro";
    std::string day  = "Friday";
    std::string food = "Pizza";
    std::string address = "121 bla avenue";

    std::ostringstream strings;
    strings << "Hello " << Name << " Today is " << day << " And your favourite food is " << food << " And you live on " << address << std::endl;

    return strings.str();
}

int basicInt(){
    int x = 6;
    int userNum;
    std::cout << "Please enter an number: "; // << echo equvillant
    std::cin >> userNum;
    std::cout << "The total numbers sum is: " << userNum+x << std::endl;
    return userNum + x; // Returning the sum
}

std::string  dates(){
    int age; 
    int born;
    double height;

    std::cout << "Please tell me your age: " << std::endl;
    std::cin  >> age;

    std::cout << "Please tell me the year you was born: " << std::endl;
    std::cin >> born;

    std::cout << "Please tell me your height now: " << std::endl;
    std::cin >> height;

    std::ostringstream datesString;
    datesString << "You were born in the year " << (born + age) << " and your height is " << height << " meters.";
    
    return datesString.str(); // Convert ostringstream to string
}