#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

// Function to pick a random name from the vector
std::string pickRandomName(const std::vector<std::string> &names)
{
    // Seed the random number generator
    std::srand(std::time(nullptr));

    // Generate a random index within the range of the vector size
    int index = std::rand() % names.size();

    // Return the randomly picked name
    return names[index];
}

int main()
{
    // Vector of names
    std::vector<std::string> names = {"Alice", "Bob", "Charlie", "David", "Eve", "Frank"};

    // Pick and print a random name
    std::cout << "Randomly picked name: " << pickRandomName(names) << std::endl;

    return 0;
}
