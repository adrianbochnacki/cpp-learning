#include <iostream>
#include <string>
#include <map>
#include <cstdlib>
#include <ctime>

using namespace std;

struct Stock
{
    string name;
    double price;
    int shares;
};

class StockMarket
{
private:
    map<string, Stock> stocks;
    map<string, int> portfolio;
    double cash;

public:
    StockMarket() : cash(1000.0)
    { // Starting cash
        srand(time(0));
        initializeStocks();
    }

    void initializeStocks()
    {
        stocks["AAPL"] = {"Apple", getRandomPrice(), 0};
        stocks["GOOG"] = {"Google", getRandomPrice(), 0};
        stocks["AMZN"] = {"Amazon", getRandomPrice(), 0};
        stocks["MSFT"] = {"Microsoft", getRandomPrice(), 0};
        stocks["TSLA"] = {"Tesla", getRandomPrice(), 0};
    }

    double getRandomPrice()
    {
        return (rand() % 900 + 100) / 10.0; // Random price between 10 and 100
    }

    void printStocks()
    {
        cout << "Available stocks:" << endl;
        for (const auto &pair : stocks)
        {
            cout << pair.first << ": " << pair.second.name << " - $" << pair.second.price << " per share" << endl;
        }
    }

    void printPortfolio()
    {
        cout << "\nYour portfolio:" << endl;
        cout << "Cash: $" << cash << endl;
        for (const auto &pair : portfolio)
        {
            if (pair.second > 0)
            {
                cout << pair.first << ": " << pair.second << " shares" << endl;
            }
        }
    }

    void buyStock(const string &symbol, int shares)
    {
        if (stocks.find(symbol) == stocks.end())
        {
            cout << "Stock not found!" << endl;
            return;
        }
        double totalCost = shares * stocks[symbol].price;
        if (totalCost > cash)
        {
            cout << "Not enough cash to buy " << shares << " shares of " << stocks[symbol].name << "!" << endl;
        }
        else
        {
            cash -= totalCost;
            portfolio[symbol] += shares;
            cout << "Bought " << shares << " shares of " << stocks[symbol].name << endl;
        }
    }

    void sellStock(const string &symbol, int shares)
    {
        if (portfolio[symbol] < shares)
        {
            cout << "Not enough shares to sell!" << endl;
        }
        else
        {
            double totalGain = shares * stocks[symbol].price;
            cash += totalGain;
            portfolio[symbol] -= shares;
            cout << "Sold " << shares << " shares of " << stocks[symbol].name << endl;
        }
    }

    void updatePrices()
    {
        for (auto &pair : stocks)
        {
            pair.second.price = getRandomPrice();
        }
    }
};

int main()
{
    StockMarket market;
    string action;
    string symbol;
    int shares;

    while (true)
    {
        market.printStocks();
        market.printPortfolio();

        cout << "\nChoose an action (buy/sell/exit): ";
        cin >> action;
        if (action == "exit")
            break;

        cout << "Enter stock symbol: ";
        cin >> symbol;
        cout << "Enter number of shares: ";
        cin >> shares;

        if (action == "buy")
        {
            market.buyStock(symbol, shares);
        }
        else if (action == "sell")
        {
            market.sellStock(symbol, shares);
        }
        else
        {
            cout << "Invalid action!" << endl;
        }

        market.updatePrices();
    }

    cout << "Exiting the stock market simulation." << endl;
    return 0;
}
