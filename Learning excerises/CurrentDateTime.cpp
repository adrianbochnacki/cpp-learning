#include <iostream>
#include <chrono>
#include <ctime>

int main()
{
    // Get the current time point
    auto now = std::chrono::system_clock::now();

    // Convert the time point to a time_t object
    std::time_t current_time = std::chrono::system_clock::to_time_t(now);

    // Convert the time_t object to a tm struct for local time
    std::tm local_time = *std::localtime(&current_time);

    // Print the current date and time
    std::cout << "Current date and time: ";
    std::cout << std::asctime(&local_time) << std::endl;

    return 0;
}
