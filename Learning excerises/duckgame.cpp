#include <iostream>
#include <cstdlib>
#include <ctime>

// Constants
const int GRID_SIZE = 5;
const int MAX_ATTEMPTS = 10;

// Function to print the grid
void printGrid(char grid[GRID_SIZE][GRID_SIZE], int playerX, int playerY)
{
    for (int i = 0; i < GRID_SIZE; ++i)
    {
        for (int j = 0; j < GRID_SIZE; ++j)
        {
            if (i == playerX && j == playerY)
            {
                std::cout << "P ";
            }
            else
            {
                std::cout << grid[i][j] << " ";
            }
        }
        std::cout << std::endl;
    }
}

// Function to move the duck randomly
void moveDuck(int &duckX, int &duckY)
{
    int direction = rand() % 4;
    switch (direction)
    {
    case 0: // Up
        if (duckX > 0)
            --duckX;
        break;
    case 1: // Down
        if (duckX < GRID_SIZE - 1)
            ++duckX;
        break;
    case 2: // Left
        if (duckY > 0)
            --duckY;
        break;
    case 3: // Right
        if (duckY < GRID_SIZE - 1)
            ++duckY;
        break;
    }
}

int main()
{
    // Seed the random number generator
    srand(static_cast<unsigned int>(time(0)));

    // Initialize the grid and positions
    char grid[GRID_SIZE][GRID_SIZE];
    for (int i = 0; i < GRID_SIZE; ++i)
    {
        for (int j = 0; j < GRID_SIZE; ++j)
        {
            grid[i][j] = '.';
        }
    }

    int playerX = GRID_SIZE / 2;
    int playerY = GRID_SIZE / 2;
    int duckX = rand() % GRID_SIZE;
    int duckY = rand() % GRID_SIZE;

    int attempts = 0;
    bool caught = false;

    std::cout << "Welcome to Catch the Duck Game!" << std::endl;

    while (attempts < MAX_ATTEMPTS && !caught)
    {
        std::cout << "\nAttempt: " << (attempts + 1) << "/" << MAX_ATTEMPTS << std::endl;
        printGrid(grid, playerX, playerY);

        std::cout << "Enter your move (WASD): ";
        char move;
        std::cin >> move;

        switch (move)
        {
        case 'W':
        case 'w':
            if (playerX > 0)
                --playerX;
            break;
        case 'S':
        case 's':
            if (playerX < GRID_SIZE - 1)
                ++playerX;
            break;
        case 'A':
        case 'a':
            if (playerY > 0)
                --playerY;
            break;
        case 'D':
        case 'd':
            if (playerY < GRID_SIZE - 1)
                ++playerY;
            break;
        default:
            std::cout << "Invalid move! Use W, A, S, or D." << std::endl;
            continue;
        }

        // Move the duck
        moveDuck(duckX, duckY);

        // Check if the player caught the duck
        if (playerX == duckX && playerY == duckY)
        {
            caught = true;
            std::cout << "Congratulations! You caught the duck!" << std::endl;
        }

        ++attempts;
    }

    if (!caught)
    {
        std::cout << "Game Over! You ran out of attempts. The duck escaped!" << std::endl;
    }

    return 0;
}
