#include <iostream>
#include <string>

using namespace std;

string getZodiacSign(int month) {
    switch (month) {
        case 1:  return "Capricorn";
        case 2:  return "Aquarius";
        case 3:  return "Pisces";
        case 4:  return "Aries";
        case 5:  return "Taurus";
        case 6:  return "Gemini";
        case 7:  return "Cancer";
        case 8:  return "Leo";
        case 9:  return "Virgo";
        case 10: return "Libra";
        case 11: return "Scorpio";
        case 12: return "Sagittarius";
        default: return "Invalid";
    }
}

bool checkCompatibility(string sign1, string sign2) {
    if ((sign1 == "Aries" && sign2 == "Leo") || 
        (sign1 == "Leo" && sign2 == "Aries") || 
        (sign1 == "Taurus" && sign2 == "Virgo") || 
        (sign1 == "Virgo" && sign2 == "Taurus") || 
        (sign1 == "Gemini" && sign2 == "Libra") || 
        (sign1 == "Libra" && sign2 == "Gemini") ||
        (sign1 == "Cancer" && sign2 == "Pisces") || 
        (sign1 == "Pisces" && sign2 == "Cancer") || 
        (sign1 == "Scorpio" && sign2 == "Cancer") || 
        (sign1 == "Cancer" && sign2 == "Scorpio") || 
        (sign1 == "Sagittarius" && sign2 == "Aries") || 
        (sign1 == "Aries" && sign2 == "Sagittarius") ||
        (sign1 == "Capricorn" && sign2 == "Taurus") || 
        (sign1 == "Taurus" && sign2 == "Capricorn") ||
        (sign1 == "Aquarius" && sign2 == "Gemini") || 
        (sign1 == "Gemini" && sign2 == "Aquarius")) {
        return true;
    } else {
        return false;
    }
}

int main() {
    int month1, month2;
    
    cout << "Enter the first month (1-12): ";
    cin >> month1;
    
    cout << "Enter the second month (1-12): ";
    cin >> month2;
    
    string sign1 = getZodiacSign(month1);
    string sign2 = getZodiacSign(month2);
    
    if (sign1 == "Invalid" || sign2 == "Invalid") {
        cout << "Invalid month entered!" << endl;
        return 1;
    }
    
    cout << "The zodiac sign for month " << month1 << " is " << sign1 << "." << endl;
    cout << "The zodiac sign for month " << month2 << " is " << sign2 << "." << endl;
    
    if (checkCompatibility(sign1, sign2)) {
        cout << sign1 << " and " << sign2 << " are compatible!" << endl;
    } else {
        cout << sign1 << " and " << sign2 << " are not very compatible." << endl;
    }
    
    return 0;
}
