#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void rollDice()
{
    // Seed the random number generator with the current time
    srand(static_cast<unsigned int>(time(0)));
    int diceRoll = rand() % 6 + 1; // Generate a random number between 1 and 6
    cout << "You rolled a " << diceRoll << "!" << endl;
}

int main()
{
    char choice;

    do
    {
        cout << "Press 'r' to roll the dice or 'q' to quit: ";
        cin >> choice;

        if (choice == 'r')
        {
            rollDice();
        }
        else if (choice == 'q')
        {
            cout << "Quitting the dice roller." << endl;
        }
        else
        {
            cout << "Invalid input. Please try again." << endl;
        }
    } while (choice != 'q');

    return 0;
}
