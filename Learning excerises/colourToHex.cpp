#include <iostream>
#include <map>
#include <string>
#include <algorithm>

// Function to convert color name to lowercase
std::string toLower(std::string str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    return str;
}

// Function to get hex value for a color
std::string getColorHex(std::string color)
{
    // Convert color name to lowercase
    color = toLower(color);

    // Map of color names to hexadecimal values
    std::map<std::string, std::string> colorMap = {
        {"red", "#FF0000"},
        {"green", "#00FF00"},
        {"blue", "#0000FF"},
        {"yellow", "#FFFF00"},
        {"orange", "#FFA500"},
        {"purple", "#800080"},
        {"pink", "#FFC0CB"},
        // Add more colors as needed
    };

    // Check if the color exists in the map
    auto it = colorMap.find(color);
    if (it != colorMap.end())
    {
        return it->second; // Return corresponding hex value
    }
    else
    {
        return "Color not found"; // If color is not found
    }
}

int main()
{
    std::string color;
    std::cout << "Enter a color (red, green, blue, yellow, orange, purple, pink): ";
    std::cin >> color;

    std::string hexValue = getColorHex(color);
    std::cout << "Hexadecimal value for " << color << " is: " << hexValue << std::endl;

    return 0;
}
