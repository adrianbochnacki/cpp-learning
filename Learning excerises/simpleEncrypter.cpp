#include <iostream>
#include <string>

using namespace std;

string encrypt(string text, int shift)
{
    string result = "";

    for (int i = 0; i < text.length(); i++)
    {
        if (isupper(text[i]))
        {
            result += char(int(text[i] + shift - 65) % 26 + 65);
        }
        else if (islower(text[i]))
        {
            result += char(int(text[i] + shift - 97) % 26 + 97);
        }
        else
        {
            result += text[i]; // Non-alphabetic characters remain unchanged
        }
    }

    return result;
}

string decrypt(string text, int shift)
{
    return encrypt(text, 26 - shift); // Decryption is just the reverse of encryption
}

int main()
{
    string text;
    int shift;
    char choice;

    cout << "Welcome to the Simple Caesar Cipher!" << endl;
    cout << "Do you want to (e)ncrypt or (d)ecrypt a message? ";
    cin >> choice;

    cout << "Enter the text: ";
    cin.ignore(); // To ignore the newline character left by cin
    getline(cin, text);

    cout << "Enter the shift (1-25): ";
    cin >> shift;

    if (choice == 'e')
    {
        cout << "Encrypted text: " << encrypt(text, shift) << endl;
    }
    else if (choice == 'd')
    {
        cout << "Decrypted text: " << decrypt(text, shift) << endl;
    }
    else
    {
        cout << "Invalid choice!" << endl;
    }

    return 0;
}
