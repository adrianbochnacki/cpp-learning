#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

bool areAnglesValid(const vector<double> &angles, int sides)
{
    double sum_of_angles = 0;
    for (double angle : angles)
    {
        sum_of_angles += angle;
    }
    // For a polygon with n sides, the sum of interior angles is (n-2)*180
    double expected_sum = (sides - 2) * 180;
    return abs(sum_of_angles - expected_sum) < 1e-6;
}

string determineShape(int sides)
{
    switch (sides)
    {
    case 3:
        return "Triangle";
    case 4:
        return "Quadrilateral";
    case 5:
        return "Pentagon";
    case 6:
        return "Hexagon";
    case 7:
        return "Heptagon";
    case 8:
        return "Octagon";
    case 9:
        return "Nonagon";
    case 10:
        return "Decagon";
    default:
        return "Unknown Shape";
    }
}

int main()
{
    int sides;
    cout << "Enter the number of sides: ";
    cin >> sides;

    if (sides < 3)
    {
        cout << "A shape must have at least 3 sides.\n";
        return 1;
    }

    vector<double> angles(sides);
    cout << "Enter the angles (in degrees): \n";
    for (int i = 0; i < sides; ++i)
    {
        cin >> angles[i];
    }

    if (areAnglesValid(angles, sides))
    {
        string shape = determineShape(sides);
        cout << "The shape is a " << shape << ".\n";
    }
    else
    {
        cout << "The angles do not form a valid shape.\n";
    }

    return 0;
}
