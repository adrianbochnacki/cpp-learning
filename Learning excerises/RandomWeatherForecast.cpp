#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

struct Weather
{
    string day;
    string condition;
    int temperature;
};

string getRandomCondition()
{
    vector<string> conditions = {"Sunny", "Cloudy", "Rainy", "Stormy", "Snowy", "Windy"};
    return conditions[rand() % conditions.size()];
}

int getRandomTemperature()
{
    return rand() % 61 - 30; // Random temperature between -30 and 30 degrees Celsius
}

void generateForecast(vector<Weather> &forecast)
{
    vector<string> days = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    for (const auto &day : days)
    {
        Weather weather;
        weather.day = day;
        weather.condition = getRandomCondition();
        weather.temperature = getRandomTemperature();
        forecast.push_back(weather);
    }
}

void printForecast(const vector<Weather> &forecast)
{
    cout << "Weekly Weather Forecast:" << endl;
    cout << "------------------------" << endl;
    for (const auto &weather : forecast)
    {
        cout << weather.day << ": " << weather.condition << ", " << weather.temperature << "°C" << endl;
    }
}

int main()
{
    srand(time(0));

    vector<Weather> forecast;
    generateForecast(forecast);
    printForecast(forecast);

    return 0;
}
