#include <iostream>
#include <vector>
#include <cmath>
#include <unistd.h> // For sleep function

using namespace std;

const int WIDTH = 80;
const int HEIGHT = 24;
const char SUN = 'O';
const char EMPTY = ' ';
const int NUM_PLANETS = 5;

struct Planet
{
    double distance;
    double angle;
    double speed;
    char symbol;
};

void initializePlanets(vector<Planet> &planets)
{
    char symbols[] = {'A', 'B', 'C', 'D', 'E'};
    for (int i = 0; i < NUM_PLANETS; ++i)
    {
        planets.push_back({5.0 + 2.0 * i, 0.0, 0.1 + 0.02 * i, symbols[i]});
    }
}

void updatePlanets(vector<Planet> &planets)
{
    for (auto &planet : planets)
    {
        planet.angle += planet.speed;
        if (planet.angle >= 2 * M_PI)
        {
            planet.angle -= 2 * M_PI;
        }
    }
}

void render(const vector<Planet> &planets)
{
    vector<vector<char>> grid(HEIGHT, vector<char>(WIDTH, EMPTY));

    // Place the sun at the center
    int centerX = WIDTH / 2;
    int centerY = HEIGHT / 2;
    grid[centerY][centerX] = SUN;

    // Place the planets
    for (const auto &planet : planets)
    {
        int x = centerX + static_cast<int>(planet.distance * cos(planet.angle));
        int y = centerY + static_cast<int>(planet.distance * sin(planet.angle));
        if (x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT)
        {
            grid[y][x] = planet.symbol;
        }
    }

    // Print the grid
    for (const auto &row : grid)
    {
        for (char cell : row)
        {
            cout << cell;
        }
        cout << endl;
    }
}

int main()
{
    vector<Planet> planets;
    initializePlanets(planets);

    for (int step = 0; step < 100; ++step)
    {
        system("clear"); // Use "cls" for Windows
        render(planets);
        updatePlanets(planets);
        usleep(200000); // Sleep for 200 milliseconds
    }

    return 0;
}
