#include <iostream>

using namespace std;

void printPyramid(int levels)
{
    for (int i = 1; i <= levels; ++i)
    {
        // Print leading spaces
        for (int j = 1; j <= (levels - i); ++j)
        {
            cout << " ";
        }
        // Print stars
        for (int k = 1; k <= (2 * i - 1); ++k)
        {
            cout << "*";
        }
        cout << endl;
    }
}

int main()
{
    int levels;
    cout << "Enter the number of levels for the pyramid: ";
    cin >> levels;

    if (levels <= 0)
    {
        cout << "Number of levels must be a positive integer." << endl;
    }
    else
    {
        printPyramid(levels);
    }

    return 0;
}
