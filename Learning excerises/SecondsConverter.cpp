#include <iostream>

using namespace std;

void convertSeconds(int totalSeconds)
{
    int days = totalSeconds / 86400; // 86400 seconds in a day
    totalSeconds %= 86400;
    int hours = totalSeconds / 3600; // 3600 seconds in an hour
    totalSeconds %= 3600;
    int minutes = totalSeconds / 60; // 60 seconds in a minute
    int seconds = totalSeconds % 60; // remaining seconds

    cout << "Converted time: "
         << days << " days, "
         << hours << " hours, "
         << minutes << " minutes, "
         << seconds << " seconds."
         << endl;
}

int main()
{
    int totalSeconds;
    cout << "Enter the number of seconds: ";
    cin >> totalSeconds;

    if (totalSeconds < 0)
    {
        cout << "Please enter a positive number of seconds." << endl;
    }
    else
    {
        convertSeconds(totalSeconds);
    }

    return 0;
}
