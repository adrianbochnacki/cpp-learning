#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Employee
{
private:
    string name;
    double hourlyRate;
    double hoursWorked; // This is a member variable

public:
    Employee(string name, double hourlyRate) : name(name), hourlyRate(hourlyRate), hoursWorked(0) {}

    // Function to set hours worked
    void setHoursWorked(double hours)
    {
        hoursWorked = hours;
    }

    // Function to calculate and return gross pay
    double calculatePay() const
    {
        return hourlyRate * hoursWorked;
    }

    // Function to get hours worked
    double getHoursWorked() const
    {
        return hoursWorked;
    }

    // Function to get employee name
    string getName() const
    {
        return name;
    }
};

// Payroll class to manage employees and calculate payroll
class Payroll
{
private:
    vector<Employee> employees;

public:
    // Function to add an employee to the system
    void addEmployee(const Employee &emp)
    {
        employees.push_back(emp);
    }

    // Function to calculate and display payroll for all employees
    void calculatePayroll() const
    {
        cout << "Payroll Information:" << endl;
        for (const auto &emp : employees)
        {
            cout << "Employee: " << emp.getName() << endl;
            cout << "Hours Worked: " << emp.getHoursWorked() << endl;
            cout << "Gross Pay: " << char(156) << emp.calculatePay() << endl;
            cout << "---------------------------" << endl;
        }
    }
};

int main()
{
    Payroll payroll;
    int option = 1;

    cout << "<------------------------------------------->" << endl;
    cout << "Welcome to the payroll system" << endl;
    while (option != 0)
    {
        string name;
        float rate;
        int hours;
        cout << "Please enter name: ";
        cin >> name;
        cout << endl
             << "Please enter hourly rate: ";
        cin >> rate;
        cout << endl
             << "Please enter the hours worked: ";
        cin >> hours;

        // Add employees to the system
        Employee emp(name, rate);  // Create an Employee object with user input
        emp.setHoursWorked(hours); // Set hours worked
        payroll.addEmployee(emp);  // Add the employee to payroll

        cout << endl
             << "Would you like to add another worker to the payroll? 0 = NO | 1 = YES ";
        cin >> option; // Update option based on user input
    }

    // Calculate and display payroll
    payroll.calculatePayroll();

    cout << "<------------------------------------------->" << endl;
    return 0;
}
