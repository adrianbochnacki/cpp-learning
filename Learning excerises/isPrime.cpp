#include <iostream>
#include <cmath>

using namespace std;

bool isPrime(int number)
{
    if (number <= 1)
    {
        return false;
    }
    if (number == 2 || number == 3)
    {
        return true;
    }
    if (number % 2 == 0 || number % 3 == 0)
    {
        return false;
    }

    for (int i = 5; i <= sqrt(number); i += 6)
    {
        if (number % i == 0 || number % (i + 2) == 0)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    int number;
    char choice;

    do
    {
        cout << "Enter a number to check if it's prime: ";
        cin >> number;

        if (isPrime(number))
        {
            cout << number << " is a prime number." << endl;
        }
        else
        {
            cout << number << " is not a prime number." << endl;
        }

        cout << "Do you want to check another number? (y/n): ";
        cin >> choice;
    } while (choice == 'y' || choice == 'Y');

    cout << "Goodbye!" << endl;

    return 0;
}
