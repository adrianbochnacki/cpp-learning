#include <iostream>
#include <vector>
#include <cstdlib>   // For rand() and srand()
#include <ctime>     // For time()
#include <algorithm> // For transform() and tolower()

int main()
{
    srand(time(nullptr)); // Seed the random number generator

    // List of words to guess
    std::vector<std::string> words = {"apple", "banana", "cherry", "orange", "pear", "grape", "pineapple"};

    // Select a random word from the list
    int index = rand() % words.size();
    std::string target = words[index];

    // Convert the target word to lowercase for case-insensitive comparison
    std::transform(target.begin(), target.end(), target.begin(), ::tolower);

    std::string guess;
    int attempts = 0;

    std::cout << "Welcome to the Guessing Game!" << std::endl;
    std::cout << "I've selected a word from a predefined list. Try to guess it!" << std::endl;

    do
    {
        std::cout << "Enter your guess: ";
        std::cin >> guess;
        std::transform(guess.begin(), guess.end(), guess.begin(), ::tolower);
        attempts++;

        if (guess != target)
        {
            std::cout << "Incorrect guess. Try again." << std::endl;
        }
    } while (guess != target);

    std::cout << "Congratulations! You guessed it right in " << attempts << " attempts." << std::endl;

    return 0;
}
