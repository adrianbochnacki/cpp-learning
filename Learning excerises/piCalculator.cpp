#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>

using namespace std;

double estimatePi(int numPoints)
{
    int pointsInsideCircle = 0;

    // Generate random points and check if they fall inside the circle
    for (int i = 0; i < numPoints; ++i)
    {
        double x = (double)rand() / RAND_MAX; // Random x coordinate between 0 and 1
        double y = (double)rand() / RAND_MAX; // Random y coordinate between 0 and 1

        // Distance from the origin
        double distance = sqrt(x * x + y * y);

        // If the point is inside the circle
        if (distance <= 1.0)
        {
            pointsInsideCircle++;
        }
    }

    // Pi is estimated as the ratio of points inside the circle to the total points
    return 4.0 * pointsInsideCircle / numPoints;
}

int main()
{
    srand(time(nullptr)); // Seed the random number generator with current time

    int numPoints;
    cout << "Enter the number of points to generate: ";
    cin >> numPoints;

    double piEstimate = estimatePi(numPoints);
    cout << "Estimated value of Pi using " << numPoints << " points: " << piEstimate << endl;

    return 0;
}
