#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;

vector<string> words = {"hangman", "computer", "programming", "cplusplus", "developer", "algorithm"};

string chooseWord() {
    srand(static_cast<unsigned int>(time(0)));
    int randomIndex = rand() % words.size();
    return words[randomIndex];
}

string hideWord(const string& word) {
    string hiddenWord(word.length(), '_');
    return hiddenWord;
}

bool checkGuess(char guess, const string& word, string& hiddenWord) {
    bool found = false;
    for (size_t i = 0; i < word.length(); ++i) {
        if (word[i] == guess) {
            hiddenWord[i] = guess;
            found = true;
        }
    }
    return found;
}

void displayHangman(int attempts) {
    if (attempts >= 1) cout << "  ____" << endl;
    if (attempts >= 2) cout << "  |  |" << endl;
    if (attempts >= 3) cout << "  |  O" << endl;
    if (attempts >= 4) cout << "  | /|\\ " << endl;
    if (attempts >= 5) cout << "  | / \\" << endl;
    if (attempts >= 6) cout << "__|__" << endl;
    cout << endl;
}

int main() {
    string word = chooseWord();
    string hiddenWord = hideWord(word);
    int maxAttempts = 6;
    int attempts = 0;
    vector<char> guessedLetters;

    cout << "Welcome to Hangman!" << endl;

    while (attempts < maxAttempts) {
        cout << "Guess the word: " << hiddenWord << endl;
        cout << "Enter your guess: ";
        char guess;
        cin >> guess;

        if (find(guessedLetters.begin(), guessedLetters.end(), guess) != guessedLetters.end()) {
            cout << "You already guessed that letter. Try again." << endl;
            continue;
        }

        guessedLetters.push_back(guess);

        if (!checkGuess(guess, word, hiddenWord)) {
            ++attempts;
            cout << "Incorrect guess! Attempts left: " << maxAttempts - attempts << endl;
        } else {
            cout << "Correct guess!" << endl;
        }

        displayHangman(attempts);

        if (hiddenWord == word) {
            cout << "Congratulations! You guessed the word: " << word << endl;
            break;
        }
    }

    if (attempts == maxAttempts) {
        cout << "You lost! The word was: " << word << endl;
    }

    return 0;
}
