#include <iostream>
#include <string>

int main(){
    std::string name;
    int number = 0; // Integer variable declaring seems to be normal 

    std::cout << "Please enter your Full name: ";
    std::getline(std::cin, name);
    std::cout << "Please enter an number: "; // << echo equvillant
    std::cin >> number; // >> user keyboard input
    std::cout << "The user name is: " << name <<  " The user has entered the number: " << number;
    return 0;
}