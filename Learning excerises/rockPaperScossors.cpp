#include <iostream>
#include <string>
#include <ctime>

using namespace std;

int CPUpickIndex()
{
    // Seed the random number generator
    srand(static_cast<unsigned int>(time(nullptr)));
    // Generate a random index
    return rand() % 3; // 3 is the size of the array
}

string getGamePieceName(int index)
{
    string gamePieces[3] = {"Rock", "Paper", "Scissors"};
    // Return the string at the specified index
    return gamePieces[index];
}

int main()
{
    int startGame = 1;
    int CPUscore = 0;
    int HumanScore = 0;
    cout << "<-------------Started--------------->" << endl;
    while (startGame == 1)
    {
        int usrOption = 0;
        int CPUoption;
        cout << "Please select from the following options: " << endl;
        cout << "Option 1: Rock" << endl;
        cout << "Option 2: Paper" << endl;
        cout << "Option 3: Scissors" << endl;
        cout << "Select Option: ";
        cin >> usrOption;
        CPUoption = CPUpickIndex();
        string usrpiece = getGamePieceName(usrOption - 1);
        string cpupiece = getGamePieceName(CPUoption);

        switch (usrOption)
        {
        case 1:
            if (cpupiece == "Rock")
            {
                cout << "You have selected " << usrpiece << ". CPU has selected " << cpupiece << endl;
                cout << "DRAW!" << endl;
            }
            else if (cpupiece == "Paper")
            {
                cout << "You have selected " << usrpiece << ". CPU has selected " << cpupiece << endl;
                cout << "YOU LOSE!" << endl;
                CPUscore++;
            }
            else if (cpupiece == "Scissors")
            {
                cout << "You have selected " << usrpiece << ". CPU has selected " << cpupiece << endl;
                cout << "YOU WIN!" << endl;
                HumanScore++;
            }
            break;
        case 2:
            if (cpupiece == "Rock")
            {
                cout << "You have selected " << usrpiece << ". CPU has selected " << cpupiece << endl;
                cout << "YOU WIN!" << endl;
                HumanScore++;
            }
            else if (cpupiece == "Paper")
            {
                cout << "You have selected " << usrpiece << ". CPU has selected " << cpupiece << endl;
                cout << "DRAW!" << endl;
            }
            else if (cpupiece == "Scissors")
            {
                cout << "You have selected " << usrpiece << ". CPU has selected " << cpupiece << endl;
                cout << "YOU LOSE!" << endl;
                CPUscore++;
            }
            break;
        case 3:
            if (cpupiece == "Rock")
            {
                cout << "You have selected " << usrpiece << ". CPU has selected " << cpupiece << endl;
                cout << "YOU LOSE!" << endl;
                CPUscore++;
            }
            else if (cpupiece == "Paper")
            {
                cout << "You have selected " << usrpiece << ". CPU has selected " << cpupiece << endl;
                cout << "YOU WIN!" << endl;
                HumanScore++;
            }
            else if (cpupiece == "Scissors")
            {
                cout << "You have selected " << usrpiece << ". CPU has selected " << cpupiece << endl;
                cout << "DRAW!" << endl;
            }
            break;
        default:
            cout << "Please enter a valid option." << endl;
            break;
        }
        if (CPUscore == 3)
        {
            startGame = 0;
            cout << "CPU WON 3/3!" << endl;
        }
        else if (HumanScore == 3)
        {
            startGame = 0;
            cout << "YOU WON 3/3!" << endl;
        }
    }
    cout << "<-------------Ended----------------->" << endl;
    return 0;
}
