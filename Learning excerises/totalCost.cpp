#include <iostream>

using namespace std;

int main()
{
    int numItems;
    double totalCost = 0;

    cout << "Welcome to the Simple Store Calculator!" << endl;
    cout << "How many items did the customer purchase? ";
    cin >> numItems;

    for (int i = 1; i <= numItems; ++i)
    {
        double quantity, price;
        cout << "Enter the quantity and price of item " << i << ": ";
        cin >> quantity >> price;
        totalCost += quantity * price;
    }

    cout << "Total cost of the items: £" << totalCost << endl;

    return 0;
}
