#include <iostream>
using namespace std;

int reload() {
    char userInput;
    cout << "Would you like to convert again? (y/n): ";
    cin >> userInput;
    if (userInput == 'y' || userInput == 'Y') {
        return 1; // Return 1 to indicate the user wants to convert again
    } else {
        cout << "Thank you! Goodbye" << endl;
        return 0; // Return 0 to indicate the user doesn't want to convert again
    }
}

int main() {
    double fahren;
    double celsius;
    int menu;
    cout << "<---------------------------------------->" << endl;
    cout << "Welcome to the temperature converter" << endl;
    
    do {
        cout << "Please select choose from the following options - " << endl;
        cout << "1 - Fahrenheit to Celsius" << endl;
        cout << "2 - Celsius to Fahrenheit " << endl;
        cout << "3 - Exit" << endl;
        cout << ": ";
        cin >> menu;

        if (menu == 1) {
            cout << "Enter Fahrenheit : ";
            cin >> fahren;
            celsius = (fahren - 32) * 5 / 9;
            cout << fahren << " *F in Celsius is " << celsius << " *C" << endl;
        } else if (menu == 2) {
            cout << "Enter Celsius : ";
            cin >> celsius;
            fahren = (celsius * 9 / 5) + 32;
            cout << celsius << " *C in Fahrenheit is " << fahren << " *F" << endl;
        } else if (menu == 3) {
            cout << "Exiting..." << endl;
            break; // Exit the loop and the program
        } else {
            cout << "Invalid option" << endl;
        }
        cout << "<-------Program Ending-------------------->" << endl;
    } while (reload()); // Loop as long as reload() returns true (1)
    
    return 0;
}
