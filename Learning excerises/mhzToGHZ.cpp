#include <iostream>
using namespace std;

// Function to convert MHz to GHz
double convertMHzToGHz(double MHz)
{
    return MHz / 1000; // 1 GHz = 1000 MHz
}

int main()
{
    double MHz;

    cout << "Enter the frequency in MHz: ";
    cin >> MHz;

    double GHz = convertMHzToGHz(MHz);
    cout << MHz << " MHz is equivalent to " << GHz << " GHz." << endl;

    return 0;
}
