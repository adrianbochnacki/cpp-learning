#include <iostream>
#include <iomanip>

using namespace std;
void convertGBPtoEuro(double gbp, double rate)
{
    double euro = gbp * rate;
    cout << fixed << setprecision(2);
    cout << gbp << " GBP is equal to " << euro << " Euros.\n";
}

void convertEurotoGBP(double euro, double rate)
{
    double gbp = euro * rate;
    cout << fixed << setprecision(2);
    cout << euro << " Euros is equal to " << gbp << " GBP.\n";
}

int main()
{
    double gbp, euro, rateGBPtoEuro, rateEurotoGBP;
    int choice;

    cout << "Currency Converter\n";
    cout << "1. Convert GBP to Euro\n";
    cout << "2. Convert Euro to GBP\n";
    cout << "Enter your choice (1 or 2): ";
    cin >> choice;

    if (choice == 1)
    {
        cout << "Enter amount in GBP: ";
        cin >> gbp;
        cout << "Enter conversion rate from GBP to Euro: ";
        cin >> rateGBPtoEuro;
        convertGBPtoEuro(gbp, rateGBPtoEuro);
    }
    else if (choice == 2)
    {
        cout << "Enter amount in Euro: ";
        cin >> euro;
        cout << "Enter conversion rate from Euro to GBP: ";
        cin >> rateEurotoGBP;
        convertEurotoGBP(euro, rateEurotoGBP);
    }
    else
    {
        cout << "Invalid choice. Please run the program again and select 1 or 2.\n";
    }

    return 0;
}
