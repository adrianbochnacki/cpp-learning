#include <iostream>

void checkBatteryStatus(float voltage)
{
    if (voltage >= 12.6)
    {
        std::cout << "The battery is good." << std::endl;
    }
    else if (voltage >= 11.8 && voltage < 12.6)
    {
        std::cout << "The battery is low." << std::endl;
    }
    else
    {
        std::cout << "The battery needs replacement." << std::endl;
    }
}

int main()
{
    float voltage;

    std::cout << "Enter the battery voltage: ";
    std::cin >> voltage;

    checkBatteryStatus(voltage);

    return 0;
}
