#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    // Seed the random number generator
    srand(time(nullptr));

    char choice;
    do
    {
        // Generate a random number (0 or 1) for heads or tails
        int result = rand() % 2;

        // Print the result
        if (result == 0)
        {
            cout << "Heads" << endl;
        }
        else
        {
            cout << "Tails" << endl;
        }

        // Ask if the user wants to toss the coin again
        cout << "Do you want to toss the coin again? (y/n): ";
        cin >> choice;
    } while (choice == 'y' || choice == 'Y');

    cout << "Thanks for tossing the coin!" << endl;

    return 0;
}
