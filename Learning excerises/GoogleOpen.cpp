#include <iostream>
#include <cstdlib> // For system()

void openBrowser(const std::string &url)
{
    std::string command;

#if defined(_WIN32) || defined(_WIN64)
    // Windows
    command = "start " + url;
#elif defined(__APPLE__)
    // macOS
    command = "open " + url;
#elif defined(__linux__)
    // Linux
    command = "xdg-open " + url;
#else
    std::cerr << "Unsupported operating system." << std::endl;
    return;
#endif

    system(command.c_str());
}

int main()
{
    std::string url = "https://www.google.com";
    openBrowser(url);
    return 0;
}
