#include <iostream>
#ifdef _WIN32
#include <windows.h>
#include <Lmcons.h>
#else
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#endif

std::string getUsername()
{
#ifdef _WIN32
    char username[UNLEN + 1];
    DWORD username_len = UNLEN + 1;
    if (GetUserName(username, &username_len))
    {
        return std::string(username);
    }
    else
    {
        return "Unknown user";
    }
#else
    char *username;
    struct passwd *pw;
    uid_t uid = geteuid();
    pw = getpwuid(uid);
    if (pw)
    {
        username = pw->pw_name;
        return std::string(username);
    }
    else
    {
        return "Unknown user";
    }
#endif
}

int main()
{
    std::string username = getUsername();
    std::cout << "You are logged in as: " << username << std::endl;
    return 0;
}
