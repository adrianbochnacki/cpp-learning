#include <iostream>
using namespace std;

int main()
{
    int month;

    cout << "<------------------------------------------->" << endl;
    cout << "Welcome to zodiac finder" << endl << endl;
    // Ask the user to input the month number
    cout << "Enter the month (1-12): ";
    cin >> month;
    cout << endl;
    // Determine the zodiac sign based on the month
    if (month < 1 || month > 12)
    {
        cout << "Invalid month number! Please enter a number between 1 and 12." << endl ;
    }
    else
    {
        switch (month)
        {
        case 1:
            cout << "Zodiac Sign: Capricorn" << endl;
            break;
        case 2:
            cout << "Zodiac Sign: Aquarius" << endl;
            break;
        case 3:
            cout << "Zodiac Sign: Pisces" << endl;
            break;
        case 4:
            cout << "Zodiac Sign: Aries" << endl;
            break;
        case 5:
            cout << "Zodiac Sign: Taurus" << endl;
            break;
        case 6:
            cout << "Zodiac Sign: Gemini" << endl;
            break;
        case 7:
            cout << "Zodiac Sign: Cancer" << endl;
            break;
        case 8:
            cout << "Zodiac Sign: Leo" << endl;
            break;
        case 9:
            cout << "Zodiac Sign: Virgo" << endl;
            break;
        case 10:
            cout << "Zodiac Sign: Libra" << endl;
            break;
        case 11:
            cout << "Zodiac Sign: Scorpio" << endl;
            break;
        case 12:
            cout << "Zodiac Sign: Sagittarius" << endl;
            break;
        default:
            break;
        }
    }
    cout << "<------------------------------------------->";
    return 0;
}
