#include <iostream>
#include <filesystem>
using namespace std;
int main()
{
    // Get the current working directory
    filesystem::path currentPath = filesystem::current_path();

    // Print the current working directory
    cout << "Current working directory: " << currentPath << endl;

    return 0;
}
