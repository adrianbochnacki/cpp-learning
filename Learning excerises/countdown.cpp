#include <iostream>
#include <thread>
#include <chrono>

using namespace std;
using namespace std::chrono;

void countdown(int seconds)
{
    while (seconds > 0)
    {
        cout << "Time remaining: " << seconds << " seconds" << endl;
        this_thread::sleep_for(seconds(1));
        --seconds;
    }
    cout << "Time's up!" << endl;
}

int main()
{
    int seconds;
    cout << "Enter the number of seconds for the countdown: ";
    cin >> seconds;

    if (seconds > 0)
    {
        countdown(seconds);
    }
    else
    {
        cout << "Please enter a positive number of seconds." << endl;
    }

    return 0;
}
