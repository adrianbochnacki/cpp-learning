#include <iostream>
#include <string>
using namespace std;

// Function to check if all characters in the string are digits
bool isNumeric(const string &cardNumber)
{
    for (size_t i = 0; i < cardNumber.length(); ++i)
    {
        if (!isdigit(cardNumber[i]))
        {
            cout << "Non-numeric character at index " << i << ": " << cardNumber[i] << ". Please retry." << endl;
            return false;  // Return false if a non-numeric character is found
        }
    }
    return true;  // Return true if all characters are numeric
}

// Function to validate the card number using Luhn's algorithm
bool validateCard(const string &cardNumber)
{
    if (!isNumeric(cardNumber))
    {
        return false;
    }

    int nDigits = cardNumber.length();
    int sum = 0;
    bool isSecond = false;

    for (int i = nDigits - 1; i >= 0; i--)
    {
        int d = cardNumber[i] - '0';

        if (isSecond)
            d = d * 2;

        // We add two digits to handle cases that make two digits after doubling
        sum += d / 10;
        sum += d % 10;

        isSecond = !isSecond;
    }
    return (sum % 10 == 0);
}

int main()
{
    cout << "<------------------------------------------>" << endl;
    cout << "This program uses the Luhn Algorithm to validate a credit card number." << endl;

    while (true)
    {
        string cardNumber;
        cout << "Please enter card number: ";
         getline(cin, cardNumber);

        if (validateCard(cardNumber))
        {
            cout << "Card number is valid." << endl;
            break;  // Break the loop if the card number is valid
        }
        else
        {
            cout << "Invalid card number. Please try again." << endl;
        }
    }

    cout << "<------------------------------------------>" << endl;
    return 0;
}
