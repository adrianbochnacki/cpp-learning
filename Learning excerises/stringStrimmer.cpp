#include <iostream>
#include <algorithm>
#include <cctype>
#include <string>

// Function to trim whitespace from the beginning of a string
std::string ltrim(const std::string &str)
{
    std::string s = str;
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch)
                                    { return !std::isspace(ch); }));
    return s;
}

// Function to trim whitespace from the end of a string
std::string rtrim(const std::string &str)
{
    std::string s = str;
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch)
                         { return !std::isspace(ch); })
                .base(),
            s.end());
    return s;
}

// Function to trim whitespace from both ends of a string
std::string trim(const std::string &str)
{
    return ltrim(rtrim(str));
}

int main()
{
    std::string input;
    std::cout << "Enter a string with whitespace: ";
    std::getline(std::cin, input);

    std::string trimmed = trim(input);
    std::cout << "Trimmed string: \"" << trimmed << "\"" << std::endl;

    return 0;
}
