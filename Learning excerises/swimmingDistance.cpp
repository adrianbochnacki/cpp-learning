#include <iostream>

int main()
{
    // Declare variables
    double breathHoldingTime;
    double swimmingSpeed = 1.5; // Average swimming speed in meters per second
    double distance;

    // Ask the user for input
    std::cout << "Enter the time (in seconds) you can hold your breath: ";
    std::cin >> breathHoldingTime;

    // Calculate the distance
    distance = swimmingSpeed * breathHoldingTime;

    // Display the result
    std::cout << "You can swim approximately " << distance << " meters while holding your breath.\n";

    return 0;
}
