#include <iostream>
#include <iomanip> // For std::setprecision

using namespace std;

// Function to calculate probability
double calculateProbability(int favorableOutcomes, int totalOutcomes)
{
    if (totalOutcomes == 0)
    {
        cerr << "Total outcomes cannot be zero." << endl;
        return 0.0;
    }
    return static_cast<double>(favorableOutcomes) / totalOutcomes;
}

int main()
{
    int favorableOutcomes;
    int totalOutcomes;

    // Input number of favorable outcomes
    cout << "Enter the number of favorable outcomes: ";
    cin >> favorableOutcomes;

    // Input total number of outcomes
    cout << "Enter the total number of outcomes: ";
    cin >> totalOutcomes;

    // Calculate probability
    double probability = calculateProbability(favorableOutcomes, totalOutcomes);

    // Output probability
    cout << fixed << setprecision(2); // Set precision to 2 decimal places
    cout << "The probability of the event occurring is: " << probability << endl;

    return 0;
}
