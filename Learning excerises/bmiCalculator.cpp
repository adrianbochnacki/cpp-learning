#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double height;
    int weight;
    double bmi;
    cout << "<--------------------------------------------->" << endl;
    cout << "Welcome to the BMI calculator" << endl;
    cout << "What is your Weight (kg) : ";
    cin >> weight;
    cout << "What is your height (cm) : ";
    cin >> height;

    // Convert height from cm to meters
    height /= 100.0;

    bmi = weight / pow(height, 2);
    cout << "Your calculated BMI is " << bmi << endl;
    cout << "<--------------------------------------------->" << endl;
    return 0;
}
