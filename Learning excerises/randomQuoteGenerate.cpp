#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

// Function to get a random quote
std::string getRandomQuote(const std::vector<std::string> &quotes)
{
    // Generate a random index
    int randomIndex = rand() % quotes.size();
    return quotes[randomIndex];
}

int main()
{
    // Seed the random number generator
    std::srand(std::time(0));

    // Vector of quotes
    std::vector<std::string> quotes = {
        "The only limit to our realization of tomorrow is our doubts of today. - Franklin D. Roosevelt",
        "Do not wait to strike till the iron is hot; but make it hot by striking. - William Butler Yeats",
        "The future belongs to those who believe in the beauty of their dreams. - Eleanor Roosevelt",
        "The best way to predict your future is to create it. - Abraham Lincoln",
        "You miss 100% of the shots you don't take. - Wayne Gretzky",
        "Whether you think you can or you think you can't, you're right. - Henry Ford",
        "I have not failed. I've just found 10,000 ways that won't work. - Thomas A. Edison",
        "Success is not the key to happiness. Happiness is the key to success. - Albert Schweitzer",
        "Keep your face always toward the sunshine—and shadows will fall behind you. - Walt Whitman"};

    // Get a random quote
    std::string randomQuote = getRandomQuote(quotes);

    // Print the random quote
    std::cout << "Random Quote: " << randomQuote << std::endl;

    return 0;
}
