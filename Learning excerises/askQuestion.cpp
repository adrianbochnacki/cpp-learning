#include <iostream>
#include <vector>

using namespace std;

struct Question
{
    string text;
    vector<string> options;
    int correctAnswer;
};

void askQuestion(const Question &q, int &score)
{
    cout << q.text << endl;
    for (int i = 0; i < q.options.size(); ++i)
    {
        cout << i + 1 << ". " << q.options[i] << endl;
    }

    int userAnswer;
    cout << "Your answer: ";
    cin >> userAnswer;

    if (userAnswer == q.correctAnswer)
    {
        cout << "Correct!" << endl;
        ++score;
    }
    else
    {
        cout << "Wrong! The correct answer is " << q.correctAnswer << ". " << q.options[q.correctAnswer - 1] << endl;
    }
    cout << endl;
}

int main()
{
    vector<Question> questions = {
        {"What is the capital of France?",
         {"1. Paris", "2. London", "3. Berlin", "4. Madrid"},
         1},
        {"What is 2 + 2?",
         {"1. 3", "2. 4", "3. 5", "4. 6"},
         2},
        {"Who wrote 'To Kill a Mockingbird'?",
         {"1. Harper Lee", "2. J.K. Rowling", "3. Mark Twain", "4. Ernest Hemingway"},
         1}};

    int score = 0;
    for (const auto &q : questions)
    {
        askQuestion(q, score);
    }

    cout << "Your final score is " << score << " out of " << questions.size() << "." << endl;

    return 0;
}
