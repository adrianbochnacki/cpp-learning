#include <iostream>
#include <string>
/*
standard out put = std::cout 
end of line      = std::endl 

*/

int main(){
    std::cout << "Hello World" << std::endl; // std::cout standard output 
    std::cout << "This is my first C++ program!";
    return 0; // Zero needed to stop code running and it means its succesful 
}