#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>

using namespace std;

// Function prototypes
string getRandomName();
int getRandomAge();
string getRandomTrait();

int main()
{
    // Initialize random seed
    srand(static_cast<unsigned int>(time(0)));

    // Generate random character details
    string name = getRandomName();
    int age = getRandomAge();
    string trait = getRandomTrait();

    // Display the character details
    cout << "Random Character Details:\n";
    cout << "Name: " << name << "\n";
    cout << "Age: " << age << "\n";
    cout << "Trait: " << trait << "\n";

    return 0;
}

// Function to generate a random name
string getRandomName()
{
    vector<string> names = {"Alice", "Bob", "Charlie", "Diana", "Edward"};
    int index = rand() % names.size();
    return names[index];
}

// Function to generate a random age between 18 and 60
int getRandomAge()
{
    return 18 + rand() % 43; // 18 to 60 inclusive
}

// Function to generate a random trait
string getRandomTrait()
{
    vector<string> traits = {"Brave", "Clever", "Kind", "Strong", "Wise"};
    int index = rand() % traits.size();
    return traits[index];
}
