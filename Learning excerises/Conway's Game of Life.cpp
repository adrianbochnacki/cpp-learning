#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <unistd.h> // For sleep function

using namespace std;

// Define the size of the grid
const int WIDTH = 20;
const int HEIGHT = 10;

// Function to initialize the grid with random cells
void initializeGrid(vector<vector<int>> &grid)
{
    srand(time(0));
    for (int y = 0; y < HEIGHT; ++y)
    {
        for (int x = 0; x < WIDTH; ++x)
        {
            grid[y][x] = rand() % 2; // Randomly assign 0 or 1
        }
    }
}

// Function to print the grid
void printGrid(const vector<vector<int>> &grid)
{
    for (int y = 0; y < HEIGHT; ++y)
    {
        for (int x = 0; x < WIDTH; ++x)
        {
            cout << (grid[y][x] ? "O" : ".") << " ";
        }
        cout << endl;
    }
}

// Function to count the alive neighbors of a cell
int countAliveNeighbors(const vector<vector<int>> &grid, int x, int y)
{
    int count = 0;
    for (int dy = -1; dy <= 1; ++dy)
    {
        for (int dx = -1; dx <= 1; ++dx)
        {
            if (dx == 0 && dy == 0)
                continue;
            int nx = x + dx;
            int ny = y + dy;
            if (nx >= 0 && ny >= 0 && nx < WIDTH && ny < HEIGHT)
            {
                count += grid[ny][nx];
            }
        }
    }
    return count;
}

// Function to update the grid to the next generation
void updateGrid(vector<vector<int>> &grid)
{
    vector<vector<int>> newGrid = grid;
    for (int y = 0; y < HEIGHT; ++y)
    {
        for (int x = 0; x < WIDTH; ++x)
        {
            int aliveNeighbors = countAliveNeighbors(grid, x, y);
            if (grid[y][x] == 1)
            {
                if (aliveNeighbors < 2 || aliveNeighbors > 3)
                {
                    newGrid[y][x] = 0; // Cell dies
                }
            }
            else
            {
                if (aliveNeighbors == 3)
                {
                    newGrid[y][x] = 1; // Cell becomes alive
                }
            }
        }
    }
    grid = newGrid;
}

int main()
{
    // Create the grid and initialize it
    vector<vector<int>> grid(HEIGHT, vector<int>(WIDTH, 0));
    initializeGrid(grid);

    // Run the game for a fixed number of generations
    const int generations = 100;
    for (int gen = 0; gen < generations; ++gen)
    {
        cout << "Generation " << gen + 1 << ":\n";
        printGrid(grid);
        updateGrid(grid);
        sleep(1);        // Wait for 1 second
        system("clear"); // Clear the screen (use "cls" for Windows)
    }

    return 0;
}
