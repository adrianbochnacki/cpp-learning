#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int RollDice()
{
    return (rand() % 6) + 1;
}

int main()
{
    srand(time(0)); // Move srand to main function
    cout << "-----------------------------------------" << endl;
    cout << "Dice Roller" << endl;
    while (true)
    {
        char option;
        cout << RollDice() << endl;
        cout << "Roll again? (Y/N): ";
        cin >> option;
        if (option == 'N' || option == 'n')
        {
            cout << "Thank you for rolling! GOODBYE" << endl;
            break;
        }
    }
    cout << "-----------------------------------------" << endl;
    return 0;
}
