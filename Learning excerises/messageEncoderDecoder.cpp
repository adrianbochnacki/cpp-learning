#include <iostream>
#include <string>

using namespace std;

// Function to encode a message using a Caesar cipher
string encode(const string &message, int shift)
{
    string encodedMessage = message;
    for (char &c : encodedMessage)
    {
        if (isalpha(c))
        {
            char base = islower(c) ? 'a' : 'A';
            c = static_cast<char>(base + (c - base + shift) % 26);
        }
    }
    return encodedMessage;
}

// Function to decode a message using a Caesar cipher
string decode(const string &encodedMessage, int shift)
{
    return encode(encodedMessage, 26 - shift);
}

int main()
{
    string message;
    int shift;

    cout << "Enter the message: ";
    getline(cin, message);

    cout << "Enter the shift value: ";
    cin >> shift;

    string encodedMessage = encode(message, shift);
    string decodedMessage = decode(encodedMessage, shift);

    cout << "Encoded message: " << encodedMessage << endl;
    cout << "Decoded message: " << decodedMessage << endl;

    return 0;
}
