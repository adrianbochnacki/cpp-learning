#include <iostream>
#include <unordered_map>
#include <string>

using namespace std;

int main()
{
    // Create a mapping from English months to Polish months
    unordered_map<string, string> monthTranslation = {
        {"January", "Styczeń"},
        {"February", "Luty"},
        {"March", "Marzec"},
        {"April", "Kwiecień"},
        {"May", "Maj"},
        {"June", "Czerwiec"},
        {"July", "Lipiec"},
        {"August", "Sierpień"},
        {"September", "Wrzesień"},
        {"October", "Październik"},
        {"November", "Listopad"},
        {"December", "Grudzień"}};

    // Input from user
    string englishMonth;
    cout << "Enter the month in English: ";
    cin >> englishMonth;

    // Convert first letter to uppercase and the rest to lowercase
    for (auto &c : englishMonth)
        c = tolower(c);
    englishMonth[0] = toupper(englishMonth[0]);

    // Find the corresponding Polish month
    auto it = monthTranslation.find(englishMonth);
    if (it != monthTranslation.end())
    {
        cout << "The month in Polish is: " << it->second << endl;
    }
    else
    {
        cout << "Invalid month entered." << endl;
    }

    return 0;
}
