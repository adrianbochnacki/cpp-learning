#include <stdio.h>

// Function to convert MPG to L/100km
double mpg_to_l_per_100km(double mpg)
{
    return 235.215 / mpg;
}

int main()
{
    double mpg, l_per_100km;

    // Ask user for input in MPG
    printf("Enter fuel efficiency in miles per gallon (MPG): ");
    scanf("%lf", &mpg);

    // Convert MPG to L/100km
    l_per_100km = mpg_to_l_per_100km(mpg);

    // Output the result
    printf("%.2f MPG is equivalent to %.2f L/100km\n", mpg, l_per_100km);

    return 0;
}
