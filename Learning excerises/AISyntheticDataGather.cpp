#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <ctime>

// Function to generate synthetic data
std::vector<std::pair<double, double>> generate_data(int n_samples, double slope, double intercept, double noise_level) {
    std::vector<std::pair<double, double>> data;
    std::srand(std::time(nullptr)); // Seed random number generator

    for (int i = 0; i < n_samples; ++i) {
        double x = static_cast<double>(i) / n_samples;
        double noise = noise_level * (static_cast<double>(std::rand()) / RAND_MAX - 0.5);
        double y = slope * x + intercept + noise;
        data.emplace_back(x, y);
    }

    return data;
}

// Linear regression model class
class LinearRegression {
public:
    LinearRegression(double lr, int n_iters) : learning_rate(lr), n_iterations(n_iters), weight(0.0), bias(0.0) {}

    // Training function
    void train(const std::vector<std::pair<double, double>>& data) {
        int n_samples = data.size();

        for (int i = 0; i < n_iterations; ++i) {
            double dw = 0.0;
            double db = 0.0;

            for (const auto& sample : data) {
                double x = sample.first;
                double y = sample.second;
                double prediction = weight * x + bias;

                // Calculate gradients
                dw += (prediction - y) * x;
                db += (prediction - y);
            }

            // Update parameters
            weight -= (learning_rate * dw) / n_samples;
            bias -= (learning_rate * db) / n_samples;
        }
    }

    // Predict function
    double predict(double x) const {
        return weight * x + bias;
    }

    // Function to evaluate model performance (Mean Squared Error)
    double evaluate(const std::vector<std::pair<double, double>>& data) const {
        double total_error = 0.0;

        for (const auto& sample : data) {
            double x = sample.first;
            double y = sample.second;
            double prediction = predict(x);
            total_error += std::pow(prediction - y, 2);
        }

        return total_error / data.size();
    }

    // Print the model parameters
    void print_parameters() const {
        std::cout << "Model parameters: Weight = " << weight << ", Bias = " << bias << std::endl;
    }

private:
    double learning_rate;
    int n_iterations;
    double weight;
    double bias;
};

int main() {
    // Generate synthetic data
    int n_samples = 100;
    double true_slope = 2.0;
    double true_intercept = 1.0;
    double noise_level = 0.1;
    auto data = generate_data(n_samples, true_slope, true_intercept, noise_level);

    // Create Linear Regression model
    double learning_rate = 0.01;
    int n_iterations = 1000;
    LinearRegression model(learning_rate, n_iterations);

    // Train the model
    model.train(data);

    // Print the model parameters
    model.print_parameters();

    // Evaluate the model
    double mse = model.evaluate(data);
    std::cout << "Mean Squared Error: " << mse << std::endl;

    // Test predictions
    double test_x = 0.5;
    double predicted_y = model.predict(test_x);
    std::cout << "Prediction for x = " << test_x << " : " << predicted_y << std::endl;

    return 0;
}
