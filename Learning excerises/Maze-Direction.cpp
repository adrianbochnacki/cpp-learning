#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>

using namespace std;

enum Direction
{
    UP,
    DOWN,
    LEFT,
    RIGHT
};

class Maze
{
public:
    Maze(int width, int height) : width(width), height(height), maze(height, vector<char>(width, '#'))
    {
        generateMaze();
    }

    void display() const
    {
        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                cout << maze[y][x];
            }
            cout << endl;
        }
    }

    bool movePlayer(Direction dir)
    {
        int newX = playerX, newY = playerY;
        switch (dir)
        {
        case UP:
            newY--;
            break;
        case DOWN:
            newY++;
            break;
        case LEFT:
            newX--;
            break;
        case RIGHT:
            newX++;
            break;
        }

        if (newX >= 0 && newX < width && newY >= 0 && newY < height && maze[newY][newX] == ' ')
        {
            maze[playerY][playerX] = ' ';
            playerX = newX;
            playerY = newY;
            maze[playerY][playerX] = 'P';
            return true;
        }
        return false;
    }

    bool reachedExit() const
    {
        return playerX == exitX && playerY == exitY;
    }

private:
    int width, height;
    int playerX, playerY;
    int exitX, exitY;
    vector<vector<char>> maze;

    void generateMaze()
    {
        srand(static_cast<unsigned int>(time(0)));
        for (int y = 1; y < height - 1; y += 2)
        {
            for (int x = 1; x < width - 1; x += 2)
            {
                maze[y][x] = ' ';
                int direction = rand() % 4;
                switch (direction)
                {
                case 0:
                    if (y > 1)
                        maze[y - 1][x] = ' ';
                    break;
                case 1:
                    if (y < height - 2)
                        maze[y + 1][x] = ' ';
                    break;
                case 2:
                    if (x > 1)
                        maze[y][x - 1] = ' ';
                    break;
                case 3:
                    if (x < width - 2)
                        maze[y][x + 1] = ' ';
                    break;
                }
            }
        }
        playerX = 1;
        playerY = 1;
        exitX = width - 2;
        exitY = height - 2;
        maze[playerY][playerX] = 'P';
        maze[exitY][exitX] = 'E';
    }
};

int main()
{
    Maze maze(11, 11);
    char command;
    cout << "Welcome to the Maze! Navigate using WASD. 'E' is the exit." << endl;

    do
    {
        maze.display();
        cout << "Enter move (WASD): ";
        cin >> command;

        switch (command)
        {
        case 'w':
            maze.movePlayer(UP);
            break;
        case 's':
            maze.movePlayer(DOWN);
            break;
        case 'a':
            maze.movePlayer(LEFT);
            break;
        case 'd':
            maze.movePlayer(RIGHT);
            break;
        }

        if (maze.reachedExit())
        {
            cout << "Congratulations! You've reached the exit!" << endl;
            break;
        }
    } while (true);

    return 0;
}
