#include <iostream>
#include <chrono>
#include <thread>

using namespace std;
using namespace std::chrono;

class Stopwatch
{
private:
    bool running;
    high_resolution_clock::time_point startTime;
    duration<double> elapsedTime;

public:
    Stopwatch() : running(false), elapsedTime(duration<double>::zero()) {}

    void start()
    {
        if (!running)
        {
            running = true;
            startTime = high_resolution_clock::now();
            cout << "Stopwatch started." << endl;
        }
        else
        {
            cout << "Stopwatch is already running." << endl;
        }
    }

    void stop()
    {
        if (running)
        {
            auto endTime = high_resolution_clock::now();
            elapsedTime += duration_cast<duration<double>>(endTime - startTime);
            running = false;
            cout << "Stopwatch stopped." << endl;
        }
        else
        {
            cout << "Stopwatch is not running." << endl;
        }
    }

    void reset()
    {
        running = false;
        elapsedTime = duration<double>::zero();
        cout << "Stopwatch reset." << endl;
    }

    void display() const
    {
        if (running)
        {
            auto currentTime = high_resolution_clock::now();
            auto currentElapsedTime = elapsedTime + duration_cast<duration<double>>(currentTime - startTime);
            cout << "Elapsed time: " << currentElapsedTime.count() << " seconds" << endl;
        }
        else
        {
            cout << "Elapsed time: " << elapsedTime.count() << " seconds" << endl;
        }
    }
};

int main()
{
    Stopwatch stopwatch;
    char command;

    do
    {
        cout << "\nStopwatch Commands:" << endl;
        cout << "s: Start" << endl;
        cout << "t: Stop" << endl;
        cout << "r: Reset" << endl;
        cout << "d: Display elapsed time" << endl;
        cout << "q: Quit" << endl;
        cout << "Enter command: ";
        cin >> command;

        switch (command)
        {
        case 's':
            stopwatch.start();
            break;
        case 't':
            stopwatch.stop();
            break;
        case 'r':
            stopwatch.reset();
            break;
        case 'd':
            stopwatch.display();
            break;
        case 'q':
            cout << "Quitting stopwatch program." << endl;
            break;
        default:
            cout << "Invalid command. Please try again." << endl;
        }

    } while (command != 'q');

    return 0;
}
