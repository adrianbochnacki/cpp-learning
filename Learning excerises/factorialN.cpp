#include <iostream>

// Function to calculate factorial of a number
unsigned long long factorial(int n)
{
    if (n < 0)
    {
        std::cerr << "Factorial is not defined for negative numbers.\n";
        return 0;
    }

    unsigned long long result = 1;
    for (int i = 1; i <= n; ++i)
    {
        result *= i;
    }
    return result;
}

int main()
{
    int number;
    std::cout << "Enter a non-negative integer: ";
    std::cin >> number;

    if (number < 0)
    {
        std::cerr << "Invalid input! Please enter a non-negative integer.\n";
        return 1;
    }

    unsigned long long result = factorial(number);
    std::cout << "Factorial of " << number << " is " << result << "\n";

    return 0;
}
