#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

struct Planet
{
    std::string name;
    double distanceFromEarth; // in millions of kilometers
};

int main()
{
    // Data source: Average distances from Earth to other planets in the solar system (in millions of kilometers)
    std::vector<Planet> planets = {
        {"Mercury", 91.7},
        {"Venus", 41.4},
        {"Mars", 78.3},
        {"Jupiter", 628.7},
        {"Saturn", 1277.4},
        {"Uranus", 2721.4},
        {"Neptune", 4347.4},
        {"Pluto", 5869.7} // Although Pluto is classified as a dwarf planet, it's included here for completeness.
    };

    std::cout << std::fixed << std::setprecision(1); // Set precision for distance values
    std::cout << "Average Distance from Earth to Other Planets in the Solar System:\n\n";

    for (const auto &planet : planets)
    {
        std::cout << std::setw(8) << planet.name << ": " << planet.distanceFromEarth << " million kilometers\n";
    }

    return 0;
}
