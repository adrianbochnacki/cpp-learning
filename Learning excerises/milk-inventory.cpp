#include <iostream>
using namespace std;

class MilkInventory
{
private:
    int bottles;

public:
    MilkInventory() : bottles(0) {}

    void addBottles(int count)
    {
        if (count > 0)
        {
            bottles += count;
            cout << count << " bottles added. Total bottles: " << bottles << endl;
        }
        else
        {
            cout << "Invalid number of bottles to add." << endl;
        }
    }

    void removeBottles(int count)
    {
        if (count > 0 && count <= bottles)
        {
            bottles -= count;
            cout << count << " bottles removed. Total bottles: " << bottles << endl;
        }
        else
        {
            cout << "Invalid number of bottles to remove." << endl;
        }
    }

    void displayBottles() const
    {
        cout << "Total bottles in inventory: " << bottles << endl;
    }
};

int main()
{
    MilkInventory inventory;
    int choice, count;

    do
    {
        cout << "\nMilk Inventory System" << endl;
        cout << "1. Add bottles" << endl;
        cout << "2. Remove bottles" << endl;
        cout << "3. Display total bottles" << endl;
        cout << "4. Exit" << endl;
        cout << "Enter your choice: ";
        cin >> choice;

        switch (choice)
        {
        case 1:
            cout << "Enter the number of bottles to add: ";
            cin >> count;
            inventory.addBottles(count);
            break;
        case 2:
            cout << "Enter the number of bottles to remove: ";
            cin >> count;
            inventory.removeBottles(count);
            break;
1        case 3:
            inventory.displayBottles();
            break;
        case 4:
            cout << "Exiting the system." << endl;
            break;
        default:
            cout << "Invalid choice. Please try again." << endl;
        }
    } while (choice != 4);

    return 0;
}
