#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <algorithm>

void generateWinningNumbers(std::vector<int> &winningNumbers, int numNumbers, int maxNumber)
{
    while (winningNumbers.size() < numNumbers)
    {
        int num = rand() % maxNumber + 1;
        if (std::find(winningNumbers.begin(), winningNumbers.end(), num) == winningNumbers.end())
        {
            winningNumbers.push_back(num);
        }
    }
}

void getUserNumbers(std::vector<int> &userNumbers, int numNumbers, int maxNumber)
{
    std::cout << "Enter " << numNumbers << " different numbers between 1 and " << maxNumber << ": ";
    int num;
    while (userNumbers.size() < numNumbers)
    {
        std::cin >> num;
        if (num >= 1 && num <= maxNumber && std::find(userNumbers.begin(), userNumbers.end(), num) == userNumbers.end())
        {
            userNumbers.push_back(num);
        }
        else
        {
            std::cout << "Invalid number or duplicate, try again: ";
        }
    }
}

int main()
{
    srand(static_cast<unsigned int>(time(0)));

    const int numNumbers = 6; // Number of numbers to pick
    const int maxNumber = 49; // Maximum number range (1 to 49)

    std::vector<int> winningNumbers;
    std::vector<int> userNumbers;

    generateWinningNumbers(winningNumbers, numNumbers, maxNumber);
    getUserNumbers(userNumbers, numNumbers, maxNumber);

    std::cout << "Winning numbers are: ";
    for (int num : winningNumbers)
    {
        std::cout << num << " ";
    }
    std::cout << std::endl;

    int matches = 0;
    for (int num : userNumbers)
    {
        if (std::find(winningNumbers.begin(), winningNumbers.end(), num) != winningNumbers.end())
        {
            matches++;
        }
    }

    std::cout << "You matched " << matches << " numbers!" << std::endl;

    if (matches == numNumbers)
    {
        std::cout << "Congratulations, you won the lottery!" << std::endl;
    }
    else
    {
        std::cout << "Better luck next time!" << std::endl;
    }

    return 0;
}
