#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    int currencyOption;
    double amountToSave;
    int months;
    double monthlySavings;
    string currencySymbol;

     // Display currency options
    cout << "Select the currency you want to save in:\n";
    cout << "1. Dollar ($)\n";
    cout << "2. Euro (€)\n";
    cout << "3. GBP (£)\n";
    cout << "Enter your choice (1-3): ";
    cin >> currencyOption;

    // Set the currency symbol based on the selected option
    switch (currencyOption) {
        case 1:
            currencySymbol = "$";
            break;
        case 2:
            currencySymbol = "€";
            break;
        case 3:
            currencySymbol = "£";
            break;
        default:
           cout << "Invalid option selected. Please restart the program.\n";
            return 1;
    }

    // Get the amount to save
   cout << "Enter the amount you wish to save: " << currencySymbol;
   cin >> amountToSave;

    // Get the number of months
   cout << "Enter the number of months you plan to save: ";
   cin >> months;

    // Calculate the monthly savings
    if (months > 0) {
        monthlySavings = amountToSave / months;

        // Display the result
       cout <<fixed <<setprecision(2);
       cout << "You need to save " << currencySymbol << monthlySavings
                  << " per month for " << months << " months to reach " 
                  << currencySymbol << amountToSave << ".\n";
    } else {
       cout << "The number of months must be greater than 0.\n";
    }

    return 0;
}
