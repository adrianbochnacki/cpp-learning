#include <iostream>
#include <cstdlib> // For rand() and srand()
#include <ctime>   // For time()

using namespace std;

int main()
{
    // Seed the random number generator
    srand(time(0));

    // Generate a random number between 0 and 4 (inclusive)
    int randomNumber = rand() % 5;

    // Define the colors
    string colors[5] = {"Red", "Blue", "Green", "Yellow", "Orange"};

    // Welcome message
    cout << "Welcome to the Guess My Color game!\n";
    cout << "I've chosen a color, try to guess it!\n\n";

    // Game loop
    bool guessedCorrectly = false;
    while (!guessedCorrectly)
    {
        cout << "Enter your guess: ";
        string guess;
        cin >> guess;

        // Check if the guess matches the chosen color
        if (guess == colors[randomNumber])
        {
            cout << "Congratulations! You guessed the color correctly!\n";
            guessedCorrectly = true;
        }
        else
        {
            cout << "Sorry, that's not the right color. Try again!\n";
        }
    }

    return 0;
}
