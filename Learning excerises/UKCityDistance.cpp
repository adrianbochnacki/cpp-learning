#include <iostream>
#include <string>
#include <iomanip>

const int NUM_CITIES = 5;
const std::string cities[NUM_CITIES] = {"London", "Edinburgh", "Newcastle", "Manchester", "Birmingham"};

// Distances between cities in kilometers
//       London, Edinburgh, Newcastle, Manchester, Birmingham
int distances[NUM_CITIES][NUM_CITIES] = {
    {0, 667, 446, 335, 205}, // London
    {667, 0, 192, 357, 395}, // Edinburgh
    {446, 192, 0, 230, 340}, // Newcastle
    {335, 357, 230, 0, 140}, // Manchester
    {205, 395, 340, 140, 0}  // Birmingham
};

void displayMenu()
{
    std::cout << "Select a city by number:\n";
    for (int i = 0; i < NUM_CITIES; i++)
    {
        std::cout << i + 1 << ". " << cities[i] << std::endl;
    }
}

int main()
{
    int startCity, endCity;
    char choice;

    do
    {
        std::cout << "Calculate the distance between two UK cities:\n";

        // Display menu for start city
        std::cout << "Choose the starting city:\n";
        displayMenu();
        std::cin >> startCity;

        // Validate input
        while (startCity < 1 || startCity > NUM_CITIES)
        {
            std::cout << "Invalid selection. Please choose a number between 1 and " << NUM_CITIES << ": ";
            std::cin >> startCity;
        }

        // Display menu for end city
        std::cout << "Choose the destination city:\n";
        displayMenu();
        std::cin >> endCity;

        // Validate input
        while (endCity < 1 || endCity > NUM_CITIES)
        {
            std::cout << "Invalid selection. Please choose a number between 1 and " << NUM_CITIES << ": ";
            std::cin >> endCity;
        }

        // Adjust for array indexing (1-5 to 0-4)
        startCity--;
        endCity--;

        if (startCity == endCity)
        {
            std::cout << "You have selected the same city for both start and destination. Distance is 0 km.\n";
        }
        else
        {
            std::cout << "The distance from " << cities[startCity] << " to " << cities[endCity]
                      << " is " << distances[startCity][endCity] << " km.\n";
        }

        std::cout << "Would you like to calculate another distance? (y/n): ";
        std::cin >> choice;

    } while (choice == 'y' || choice == 'Y');

    std::cout << "Thank you for using the distance calculator!\n";

    return 0;
}
