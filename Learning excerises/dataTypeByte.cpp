#include <iostream>
using namespace std;

int main(){
    // Find data type fundamental byte size
    cout << "------------------------------------------\n"; 
        cout << "The sizeof(char) is : "     << sizeof(char) << " Bytes \n";
        cout << "The sizeof(char) is : "     << sizeof(char) << " Bytes \n";
        cout << "The sizeof(short) is : "    << sizeof(short) << " Bytes \n";
        cout << "The sizeof(int) is : "      << sizeof(int) << " Bytes \n";
        cout << "The sizeof(long) is : "     << sizeof(long) << " Bytes \n";
        cout << "The sizeof(long long) is : " << sizeof(long long) << " Bytes \n";
        cout << "The sizeof(float) is : "    << sizeof(float) << " Bytes \n";
        cout << "The sizeof(double) is : "   << sizeof(double) << " Bytes \n";
        cout << "The sizeof(bool) is : "     << sizeof(bool) << " Bytes \n";
        cout << "The sizeof(string) is : "   << sizeof(string) << " Bytes \n";
    cout << "------------------------------------------\n"; 
    return 0;
}