#include <iostream>
#include <climits>

using namespace std;

int main()
{

    cout << "Check The upper and lowwer limits of integer :\n";
    cout << "--------------------------------------------------\n";
    cout << " The maximum limit of int data type :                  " << INT_MAX << endl;
    cout << " The minimum limit of int data type :                  " << INT_MIN << endl;
    cout << " The maximum limit of unsigned int data type :         " << UINT_MAX << endl;
    cout << " The maximum limit of long long data type :            " << LLONG_MAX << endl;
    cout << " The minimum limit of long long data type :            " << LLONG_MIN << endl;
    cout << " The maximum limit of unsigned char data type :        " << UCHAR_MAX << endl;
    cout << " The minimum limit of short data type :                " << SHRT_MIN << endl;
    cout << " The maximum limit of short data type :                " << SHRT_MAX << endl;
    cout << " The maximum limit of unsigned short data type :       " << USHRT_MAX << endl;
    cout << "--------------------------------------------------\n";
    return 0;
}