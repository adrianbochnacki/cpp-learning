#include <iostream>
#include <sstream>
#include <algorithm>

using namespace std;

string reverseWord(string word)
{
    reverse(word.begin(), word.end());
    return word;
}

int main()
{
    string sentence, word, result;
    cout << "Enter a sentence: ";
    getline(cin, sentence);

    stringstream ss(sentence);

    while (ss >> word)
    {
        result += reverseWord(word) + " ";
    }

    // Remove the trailing space
    result = result.substr(0, result.length() - 1);

    cout << "Reversed words: " << result << endl;

    return 0;
}
