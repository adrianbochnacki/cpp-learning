#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>


class Color
{
public:
    int red;
    int green;
    int blue;

    Color(int r, int g, int b) : red(r), green(g), blue(b) {}
};

int hexToDec(const std::string &hex)
{
    int value;
    std::istringstream iss(hex);
    iss >> std::hex >> value;
    return value;
}

Color hexToRGB(const std::string &hex)
{
    if (hex[0] != '#' || hex.length() != 7)
    {
        throw std::invalid_argument("Invalid hex color code");
    }

    int r = hexToDec(hex.substr(1, 2));
    int g = hexToDec(hex.substr(3, 2));
    int b = hexToDec(hex.substr(5, 2));

    return Color(r, g, b);
}

void printColor(const Color &color)
{
    std::cout << "RGB(" << color.red << ", " << color.green << ", " << color.blue << ")" << std::endl;
}

int main()
{
    std::string hexCode;
    std::cout << "Enter a hex color code (e.g., #FFFFFF): ";
    std::cin >> hexCode;

    try
    {
        Color color = hexToRGB(hexCode);
        printColor(color);
    }
    catch (const std::exception &e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
    }

    return 0;
}
