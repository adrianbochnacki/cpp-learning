#include <iostream>
#include <vector>

using namespace std;

void generateFibonacci(int n)
{
    vector<int> fibonacci(n);
    if (n > 0)
        fibonacci[0] = 0;
    if (n > 1)
        fibonacci[1] = 1;

    for (int i = 2; i < n; ++i)
    {
        fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
    }

    cout << "Fibonacci sequence up to " << n << " terms:" << endl;
    for (int i = 0; i < n; ++i)
    {
        cout << fibonacci[i] << " ";
    }
    cout << endl;
}

int main()
{
    int terms;
    cout << "Enter the number of terms: ";
    cin >> terms;

    if (terms <= 0)
    {
        cout << "Number of terms should be a positive integer." << endl;
    }
    else
    {
        generateFibonacci(terms);
    }

    return 0;
}
