#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

using namespace std;

string generatePassword(int length)
{
    const string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[]{}|;:'\",.<>?/";
    string password;

    for (int i = 0; i < length; ++i)
    {
        password += characters[rand() % characters.size()];
    }

    return password;
}

int main()
{
    srand(static_cast<unsigned int>(time(0))); // Seed the random number generator with the current time

    int length;
    cout << "Enter the desired password length: ";
    cin >> length;

    if (length <= 0)
    {
        cout << "Password length should be a positive integer." << endl;
    }
    else
    {
        string password = generatePassword(length);
        cout << "Generated password: " << password << endl;
    }

    return 0;
}
