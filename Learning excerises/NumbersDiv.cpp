#include <iostream>

int main()
{
    int num;

    // Ask the user to input a number
    std::cout << "Enter a number: ";
    std::cin >> num;

    // Check and print all numbers from 1 to num that are divisible by num
    std::cout << "Numbers divisible by " << num << " without remainder are: ";
    for (int i = 1; i <= num; ++i)
    {
        if (i % num == 0)
        {
            std::cout << i << " ";
        }
    }

    std::cout << std::endl;
    return 0;
}
