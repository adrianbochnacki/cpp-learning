#include <iostream>

// Function to generate Fibonacci sequence recursively
void fibonacci(int n, int a = 0, int b = 1)
{
    if (n > 0)
    {
        std::cout << a << " ";
        fibonacci(n - 1, b, a + b);
    }
}

int main()
{
    int limit;

    // Prompt the user to enter the limit for Fibonacci sequence
    std::cout << "Enter the limit for Fibonacci sequence: ";
    std::cin >> limit;

    // Print Fibonacci sequence up to the specified limit
    std::cout << "Fibonacci sequence up to " << limit << " is:\n";
    fibonacci(limit);

    return 0;
}
