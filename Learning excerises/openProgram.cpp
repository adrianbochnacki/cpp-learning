#include <windows.h>
#include <iostream>
#include <string>

using namespace std;

int main() {
    // Use std::string to handle the input path
    string program;

    // Prompt user for the path to the Microsoft Word executable
    cout << "Enter the full path of the program to open: ";
    getline(cin, program);

    // Convert std::string to a C-style string
    const char* programCStr = program.c_str();

    // Open the specified program
    ShellExecute(NULL, "open", programCStr, NULL, NULL, SW_SHOWNORMAL);

    return 0;
}
