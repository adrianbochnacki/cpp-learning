#include <iostream>
#include <vector>
#include <string>

using namespace std;

// Function to draw the Tic Tac Toe board
void drawBoard(const vector<vector<char>> &board)
{
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            cout << board[i][j];
            if (j < 2)
                cout << " | ";
        }
        cout << endl;
        if (i < 2)
            cout << "---------" << endl;
    }
}

// Function to check if someone has won
bool checkWin(const vector<vector<char>> &board, char player)
{
    // Check rows and columns
    for (int i = 0; i < 3; ++i)
    {
        if (board[i][0] == player && board[i][1] == player && board[i][2] == player)
            return true;
        if (board[0][i] == player && board[1][i] == player && board[2][i] == player)
            return true;
    }
    // Check diagonals
    if (board[0][0] == player && board[1][1] == player && board[2][2] == player)
        return true;
    if (board[0][2] == player && board[1][1] == player && board[2][0] == player)
        return true;
    return false;
}

int main()
{
    vector<vector<char>> board(3, vector<char>(3, ' ')); // Initialise an empty board

    bool player1Turn = true;
    int moves = 0;

    while (moves < 9)
    {
        cout << "Current Board:" << endl;
        drawBoard(board);

        int row, col;
        char symbol = (player1Turn) ? 'X' : 'O';

        cout << "Player " << ((player1Turn) ? "1" : "2") << " (" << symbol << "), enter your move (row and column): ";
        cin >> row >> col;

        if (row < 0 || row > 2 || col < 0 || col > 2 || board[row][col] != ' ')
        {
            cout << "Invalid move. Please try again." << endl;
            continue;
        }

        board[row][col] = symbol;
        moves++;

        if (checkWin(board, symbol))
        {
            cout << "Player " << ((player1Turn) ? "1" : "2") << " wins!" << endl;
            drawBoard(board);
            break;
        }

        player1Turn = !player1Turn; // Switch turns
    }

    if (moves == 9)
    {
        cout << "It's a draw!" << endl;
        drawBoard(board);
    }

    return 0;
}
