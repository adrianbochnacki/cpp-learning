#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>

using namespace std;

// Function to calculate the difference in days between two dates
int calculateDaysDifference(tm birthday, tm today)
{
    auto birthday_time = mktime(&birthday);
    auto today_time = mktime(&today);
    return difftime(today_time, birthday_time) / (60 * 60 * 24);
}

// Function to calculate age in dog years
int calculateDogYears(int humanYears)
{
    return humanYears * 7;
}

int main()
{
    // Fetch and display the current system date and time
    time_t now = time(0);
    tm *currentTime = localtime(&now);
    cout << "Current Date and Time: " << put_time(currentTime, "%Y-%m-%d %H:%M:%S") << endl;

    // Prompt the user to enter their birthday
    int year, month, day;
    cout << "Enter your birthday (YYYY MM DD): ";
    cin >> year >> month >> day;

    // Set up the birthday tm structure
    tm userBirthday = {0};
    userBirthday.tm_year = year - 1900; // tm_year is years since 1900
    userBirthday.tm_mon = month - 1;    // tm_mon is 0-based
    userBirthday.tm_mday = day;

    // Calculate the difference in days from the user's birthday to today
    int daysDifference = calculateDaysDifference(userBirthday, *currentTime);
    cout << "Days from your birthday to today: " << daysDifference << " days" << endl;

    // Calculate the user's age in years
    int ageYears = daysDifference / 365; // Rough estimate, doesn't account for leap years
    cout << "Your age in human years: " << ageYears << " years" << endl;

    // Calculate the user's age in dog years
    int dogYears = calculateDogYears(ageYears);
    cout << "Your age in dog years: " << dogYears << " years" << endl;

    return 0;
}
