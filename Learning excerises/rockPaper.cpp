#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

string getComputerChoice()
{
    const string choices[] = {"Rock", "Paper", "Scissors"};
    int randomIndex = rand() % 3; // Random number between 0 and 2
    return choices[randomIndex];
}

string getUserChoice()
{
    string choice;
    cout << "Enter your choice (Rock, Paper, or Scissors): ";
    cin >> choice;
    return choice;
}

void determineWinner(const string &userChoice, const string &computerChoice)
{
    if (userChoice == computerChoice)
    {
        cout << "It's a tie! You both chose " << userChoice << "." << endl;
    }
    else if ((userChoice == "Rock" && computerChoice == "Scissors") ||
             (userChoice == "Paper" && computerChoice == "Rock") ||
             (userChoice == "Scissors" && computerChoice == "Paper"))
    {
        cout << "You win! " << userChoice << " beats " << computerChoice << "." << endl;
    }
    else
    {
        cout << "You lose! " << computerChoice << " beats " << userChoice << "." << endl;
    }
}

int main()
{
    srand(static_cast<unsigned int>(time(0))); // Seed random number generator

    string userChoice = getUserChoice();
    string computerChoice = getComputerChoice();

    cout << "Computer chose: " << computerChoice << endl;
    determineWinner(userChoice, computerChoice);

    return 0;
}
