#include <iostream>
#include <vector>
#include <stack>
#include <cstdlib>
#include <ctime>

using namespace std;

enum Direction
{
    UP,
    DOWN,
    LEFT,
    RIGHT
};

struct Cell
{
    int x, y;
    bool visited;
    bool topWall, bottomWall, leftWall, rightWall;

    Cell(int x, int y) : x(x), y(y), visited(false), topWall(true), bottomWall(true), leftWall(true), rightWall(true) {}
};

void initializeMaze(vector<vector<Cell>> &maze, int width, int height)
{
    for (int y = 0; y < height; ++y)
    {
        vector<Cell> row;
        for (int x = 0; x < width; ++x)
        {
            row.push_back(Cell(x, y));
        }
        maze.push_back(row);
    }
}

bool isValid(int x, int y, int width, int height)
{
    return (x >= 0 && y >= 0 && x < width && y < height);
}

void generateMaze(vector<vector<Cell>> &maze, int width, int height)
{
    stack<Cell *> cellStack;
    Cell *currentCell = &maze[0][0];
    currentCell->visited = true;
    int totalCells = width * height;
    int visitedCells = 1;

    srand(time(0));

    while (visitedCells < totalCells)
    {
        vector<Direction> neighbors;

        // Check all four directions
        if (isValid(currentCell->x, currentCell->y - 1, width, height) && !maze[currentCell->y - 1][currentCell->x].visited)
            neighbors.push_back(UP);
        if (isValid(currentCell->x, currentCell->y + 1, width, height) && !maze[currentCell->y + 1][currentCell->x].visited)
            neighbors.push_back(DOWN);
        if (isValid(currentCell->x - 1, currentCell->y, width, height) && !maze[currentCell->y][currentCell->x - 1].visited)
            neighbors.push_back(LEFT);
        if (isValid(currentCell->x + 1, currentCell->y, width, height) && !maze[currentCell->y][currentCell->x + 1].visited)
            neighbors.push_back(RIGHT);

        if (!neighbors.empty())
        {
            Direction direction = neighbors[rand() % neighbors.size()];

            switch (direction)
            {
            case UP:
                maze[currentCell->y - 1][currentCell->x].visited = true;
                currentCell->topWall = false;
                maze[currentCell->y - 1][currentCell->x].bottomWall = false;
                currentCell = &maze[currentCell->y - 1][currentCell->x];
                break;
            case DOWN:
                maze[currentCell->y + 1][currentCell->x].visited = true;
                currentCell->bottomWall = false;
                maze[currentCell->y + 1][currentCell->x].topWall = false;
                currentCell = &maze[currentCell->y + 1][currentCell->x];
                break;
            case LEFT:
                maze[currentCell->y][currentCell->x - 1].visited = true;
                currentCell->leftWall = false;
                maze[currentCell->y][currentCell->x - 1].rightWall = false;
                currentCell = &maze[currentCell->y][currentCell->x - 1];
                break;
            case RIGHT:
                maze[currentCell->y][currentCell->x + 1].visited = true;
                currentCell->rightWall = false;
                maze[currentCell->y][currentCell->x + 1].leftWall = false;
                currentCell = &maze[currentCell->y][currentCell->x + 1];
                break;
            }

            cellStack.push(currentCell);
            visitedCells++;
        }
        else
        {
            currentCell = cellStack.top();
            cellStack.pop();
        }
    }
}

void printMaze(const vector<vector<Cell>> &maze, int width, int height)
{
    for (int y = 0; y < height; ++y)
    {
        // Print top walls
        for (int x = 0; x < width; ++x)
        {
            cout << (maze[y][x].topWall ? "+---" : "+   ");
        }
        cout << "+" << endl;

        // Print left walls and spaces
        for (int x = 0; x < width; ++x)
        {
            cout << (maze[y][x].leftWall ? "|   " : "    ");
        }
        cout << "|" << endl;
    }

    // Print bottom walls
    for (int x = 0; x < width; ++x)
    {
        cout << "+---";
    }
    cout << "+" << endl;
}

int main()
{
    int width, height;
    cout << "Enter maze width: ";
    cin >> width;
    cout << "Enter maze height: ";
    cin >> height;

    vector<vector<Cell>> maze;
    initializeMaze(maze, width, height);
    generateMaze(maze, width, height);
    printMaze(maze, width, height);

    return 0;
}
