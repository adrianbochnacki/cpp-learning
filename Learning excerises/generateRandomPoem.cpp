#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;

vector<string> adjectives = {"blue", "silent", "lonely", "bright", "dark", "ancient", "serene"};
vector<string> nouns = {"sky", "forest", "ocean", "mountain", "river", "star", "dream"};
vector<string> verbs = {"whispers", "sings", "dances", "shines", "grows", "glows", "calls"};
vector<string> adverbs = {"softly", "brightly", "quietly", "slowly", "boldly", "silently", "gracefully"};

string getRandomWord(const vector<string> &words)
{
    return words[rand() % words.size()];
}

void generatePoem()
{
    cout << "Here is your randomly generated poem:\n"
         << endl;

    string line1 = "The " + getRandomWord(adjectives) + " " + getRandomWord(nouns) + " " + getRandomWord(verbs) + " " + getRandomWord(adverbs) + ".";
    string line2 = "A " + getRandomWord(adjectives) + " " + getRandomWord(nouns) + " " + getRandomWord(verbs) + " " + getRandomWord(adverbs) + ".";
    string line3 = "Under the " + getRandomWord(adjectives) + " " + getRandomWord(nouns) + ".";
    string line4 = "And the " + getRandomWord(nouns) + " " + getRandomWord(verbs) + " " + getRandomWord(adverbs) + ".";

    cout << line1 << endl;
    cout << line2 << endl;
    cout << line3 << endl;
    cout << line4 << endl;
}

int main()
{
    srand(time(0));
    generatePoem();
    return 0;
}
