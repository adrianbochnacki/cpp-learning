#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void magic8Ball()
{
    const string responses[] = {
        "It is certain.",
        "It is decidedly so.",
        "Without a doubt.",
        "Yes – definitely.",
        "You may rely on it.",
        "As I see it, yes.",
        "Most likely.",
        "Outlook good.",
        "Yes.",
        "Signs point to yes.",
        "Reply hazy, try again.",
        "Ask again later.",
        "Better not tell you now.",
        "Cannot predict now.",
        "Concentrate and ask again.",
        "Don't count on it.",
        "My reply is no.",
        "My sources say no.",
        "Outlook not so good.",
        "Very doubtful."};

    srand(static_cast<unsigned int>(time(0))); // Seed random number generator

    cout << "Welcome to the Magic 8-Ball!" << endl;
    cout << "Ask a yes/no question: ";
    string question;
    getline(cin, question); // Read the entire question from the user

    int randomIndex = rand() % 20; // Generate a random number between 0 and 19
    cout << "The Magic 8-Ball says: " << responses[randomIndex] << endl;
}

int main()
{
    char choice;

    do
    {
        magic8Ball();
        cout << "\nDo you want to ask another question? (y/n): ";
        cin >> choice;
        cin.ignore(); // Ignore the newline character left in the input buffer
    } while (choice == 'y' || choice == 'Y');

    cout << "Goodbye!" << endl;
    return 0;
}
