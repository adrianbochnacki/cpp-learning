#include <iostream>
#include <string>
#include <cstdlib> // For rand() and srand()
#include <ctime>   // For time()
using namespace std;

string generateRandomPassword(int length)
{
    // Define character sets for each type of character
    string lowercaseChars = "abcdefghijklmnopqrstuvwxyz";
    string uppercaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    string numericChars = "0123456789";
    string specialChars = "!@#$%^&*()_+-=[]{}|;:,.<>?";

    // Combine all character sets into one string
    string allChars = lowercaseChars + uppercaseChars + numericChars + specialChars;

    // Seed the random number generator
    srand(time(nullptr));

    // Initialise the password string
    string password;

    // Generate random characters and append them to the password string
    for (int i = 0; i < length; ++i)
    {
        // Generate a random index within the range of allChars
        int index = rand() % allChars.length();
        // Append the character at the random index to the password string
        password += allChars[index];
    }

    return password;
}
int main()
{
    string password;
    int PassLen;
    cout << "Enter Password Length: ";
    cin >> PassLen;
    password = generateRandomPassword(PassLen);
    cout << password;
    return 0;
}