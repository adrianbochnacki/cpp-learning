#include <iostream>
#include <iomanip>

using namespace std;

// Function to convert advertised size (decimal GB) to actual size (binary GiB)
double calculateActualUsableSpace(double advertisedSizeGB)
{
    // Convert GB to bytes (1 GB = 1,000,000,000 bytes)
    double sizeInBytes = advertisedSizeGB * 1'000'000'000;
    // Convert bytes to GiB (1 GiB = 1,073,741,824 bytes)
    double actualSizeGiB = sizeInBytes / (1'073'741'824);
    return actualSizeGiB;
}

int main()
{
    double advertisedSizeGB;

    cout << "Enter the advertised size of the hard drive in GB: ";
    cin >> advertisedSizeGB;

    double actualSizeGiB = calculateActualUsableSpace(advertisedSizeGB);

    cout << fixed << setprecision(2);
    cout << "Advertised size: " << advertisedSizeGB << " GB" << endl;
    cout << "Actual usable size: " << actualSizeGiB << " GiB" << endl;

    return 0;
}
