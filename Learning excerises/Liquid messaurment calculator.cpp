#include <iostream>
using namespace std;

// Conversion constants
const double LITERS_TO_MILLILITERS = 1000.0;
const double LITERS_TO_CUBIC_METERS = 0.001;
const double LITERS_TO_GALLONS_US = 0.264172;
const double LITERS_TO_CUPS_US = 4.22675;

// Function to convert liters to other measurements
void convertLiters(double liters)
{
    double milliliters = liters * LITERS_TO_MILLILITERS;
    double cubicMeters = liters * LITERS_TO_CUBIC_METERS;
    double gallonsUS = liters * LITERS_TO_GALLONS_US;
    double cupsUS = liters * LITERS_TO_CUPS_US;

    cout << liters << " liters is equal to:" << endl;
    cout << milliliters << " milliliters" << endl;
    cout << cubicMeters << " cubic meters" << endl;
    cout << gallonsUS << " gallons (US)" << endl;
    cout << cupsUS << " cups (US)" << endl;
}

int main()
{
    double liters;

    // Input from user
    cout << "Enter the amount in liters: ";
    cin >> liters;

    // Perform conversion
    convertLiters(liters);

    return 0;
}
