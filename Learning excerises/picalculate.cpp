#include <iostream>
#include <iomanip>

int main(){
    const double PI = 3.14159;
    double radius = 10;
    double circumference = 2 * PI * radius;

  // Set precision to 2 decimal places
    std::cout << std::fixed << std::setprecision(2) << circumference << "cm";

    return 0;
}