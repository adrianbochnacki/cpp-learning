#include <iostream>
#include <windows.h>

using namespace std;

void ListDrives()
{
    DWORD driveMask = GetLogicalDrives();
    if (driveMask == 0)
    {
        cerr << "GetLogicalDrives() failed with error code: " << GetLastError() << endl;
        return;
    }

    char driveLetter = 'A';
    while (driveMask)
    {
        if (driveMask & 1)
        {
            cout << driveLetter << ":\\ Active" << endl;
        }
        driveMask >>= 1;
        driveLetter++;
    }
}

int main()
{
    ListDrives();
    return 0;
}
