#include <iostream>
#include <comutil.h>
#include <comdef.h>
#include <Windows.h>
#include <atlbase.h>
#include <atlcom.h>
#include <atlconv.h>
#include <msword.olb>

using namespace std;

int main() {
    CoInitialize(NULL);

    try {
        // Create an instance of Word Application
        _ApplicationPtr pWordApp(__uuidof(Word::Application));
        
        // Disable alerts to prevent any prompts during the process
        pWordApp->PutDisplayAlerts(Word::WdAlertLevel::wdAlertsNone);

        // Open the Word document
        _DocumentPtr pDoc = pWordApp->Documents->Open(_bstr_t("your_word_document.docx"));

        // Save the document as HTML
        _bstr_t htmlFileName = _bstr_t("output.html");
        pDoc->SaveAs(htmlFileName, Word::WdSaveFormat::wdFormatHTML);

        // Close the document
        pDoc->Close(Word::WdSaveOptions::wdDoNotSaveChanges);

        // Quit Word Application
        pWordApp->Quit();
    } 
    catch (_com_error& e) {
        cout << "Error: " << e.ErrorMessage() << endl;
    }

    CoUninitialize();
    return 0;
}
