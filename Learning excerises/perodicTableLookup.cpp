#include <iostream>
#include <string>
#include <map>

using namespace std;
// Define a struct to store element information
struct Element
{
    int atomicNumber;
    string symbol;
    string name;
    double atomicWeight;
};

map<string, Element> initialisePeriodicTable()
{
    map<string, Element> periodicTable;

    // Top 10 elements
    periodicTable["H"] = {1, "H", "Hydrogen", 1.008};
    periodicTable["He"] = {2, "He", "Helium", 4.0026};
    periodicTable["Li"] = {3, "Li", "Lithium", 6.94};
    periodicTable["Be"] = {4, "Be", "Beryllium", 9.0122};
    periodicTable["B"] = {5, "B", "Boron", 10.81};
    periodicTable["C"] = {6, "C", "Carbon", 12.011};
    periodicTable["N"] = {7, "N", "Nitrogen", 14.007};
    periodicTable["O"] = {8, "O", "Oxygen", 15.999};
    periodicTable["F"] = {9, "F", "Fluorine", 18.998};
    periodicTable["Ne"] = {10, "Ne", "Neon", 20.180};
    return periodicTable;
}

// Function to look up an element by its symbol
Element lookupElement(const string &symbol, const map<string, Element> &periodicTable)
{
    if (periodicTable.find(symbol) != periodicTable.end())
    {
        return periodicTable.at(symbol);
    }
    else
    {
        // Return a default element if not found
        return {0, "", "Unknown", 0.0};
    }
}

int main()
{
    // Initialise the periodic table
    map<string, Element> periodicTable = initialisePeriodicTable();

    // Get an element symbol from the user
    string elementSymbol;
    cout << "Enter the symbol of the element: ";
    cin >> elementSymbol;

    // Look up the element
    Element element = lookupElement(elementSymbol, periodicTable);

    // Print the element information
    if (element.atomicNumber != 0)
    {
        cout << "Element Symbol: " << element.symbol << "\n";
        cout << "Atomic Number: " << element.atomicNumber << "\n";
        cout << "Name: " << element.name << "\n";
        cout << "Atomic Weight: " << element.atomicWeight << "\n";
    }
    else
    {
        cout << "Element not found.\n";
    }

    return 0;
}
