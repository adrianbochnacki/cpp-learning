#include <iostream>
#include <string>
#include <bitset>

using namespace std;

void stringToBinary(const string &input)
{
    for (char c : input)
    {
        bitset<8> binary(c);
        cout << binary << ' ';
    }
    cout << endl;
}

int main()
{
    string input;
    cout << "Enter a string: ";
    getline(cin, input);

    cout << "Binary representation: ";
    stringToBinary(input);

    return 0;
}
