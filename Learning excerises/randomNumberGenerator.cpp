#include <iostream>
#include <random>
#include <chrono> // for seeding

int main()
{
    // Use current time as seed for random number generator
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 eng(seed);

    // Define the range for random numbers
    int min = 1;
    int max = 100;

    // Define a distribution
    std::uniform_int_distribution<> distr(min, max);

    // Generate and print a random number
    int random_number = distr(eng);
    std::cout << "Random number: " << random_number << std::endl;

    return 0;
}
