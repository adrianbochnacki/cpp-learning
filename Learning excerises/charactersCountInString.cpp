#include <iostream>
#include <string>
using namespace std;

// Function to count occurrences of a character in a string
int countCharacter(const string& str, char character) {
    int count = 0;
    for (char c : str) {
        if (c == character) {
            count++;
        }
    }
    return count;
}

int main() {
    string input;
    char character;

    // Get the string and the character to count from the user
    cout << "Enter a string: ";
    getline(cin, input);
    
    cout << "Enter the character to count: ";
    cin >> character;

    // Count occurrences of the character and output the result
    int result = countCharacter(input, character);
    cout << "The character '" << character << "' appears " << result << " times in the string." << endl;

    return 0;
}
