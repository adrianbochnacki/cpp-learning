#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;

class BankAccount
{
public:
    BankAccount(string owner, double initialDeposit)
    {
        this->owner = owner;
        balance = initialDeposit;
    }

    void deposit(double amount)
    {
        balance += amount;
        cout << "Deposited $" << amount << ". New balance: $" << balance << endl;
    }

    void withdraw(double amount)
    {
        if (amount > balance)
        {
            cout << "Insufficient funds. Withdrawal failed." << endl;
        }
        else
        {
            balance -= amount;
            cout << "Withdrew $" << amount << ". New balance: $" << balance << endl;
        }
    }

    void checkBalance() const
    {
        cout << "Current balance: $" << balance << endl;
    }

private:
    string owner;
    double balance;
};

int main()
{
    unordered_map<string, BankAccount> accounts;

    while (true)
    {
        cout << "\nWelcome to the Banking System\n";
        cout << "1. Create an account\n";
        cout << "2. Deposit money\n";
        cout << "3. Withdraw money\n";
        cout << "4. Check balance\n";
        cout << "5. Exit\n";
        cout << "Choose an option: ";
        int option;
        cin >> option;

        if (option == 1)
        {
            cout << "Enter your name: ";
            string name;
            cin >> name;
            cout << "Enter initial deposit: ";
            double initialDeposit;
            cin >> initialDeposit;
            accounts[name] = BankAccount(name, initialDeposit);
            cout << "Account created for " << name << " with initial deposit of $" << initialDeposit << endl;
        }
        else if (option == 2)
        {
            cout << "Enter your name: ";
            string name;
            cin >> name;
            if (accounts.find(name) != accounts.end())
            {
                cout << "Enter amount to deposit: ";
                double amount;
                cin >> amount;
                accounts[name].deposit(amount);
            }
            else
            {
                cout << "Account not found." << endl;
            }
        }
        else if (option == 3)
        {
            cout << "Enter your name: ";
            string name;
            cin >> name;
            if (accounts.find(name) != accounts.end())
            {
                cout << "Enter amount to withdraw: ";
                double amount;
                cin >> amount;
                accounts[name].withdraw(amount);
            }
            else
            {
                cout << "Account not found." << endl;
            }
        }
        else if (option == 4)
        {
            cout << "Enter your name: ";
            string name;
            cin >> name;
            if (accounts.find(name) != accounts.end())
            {
                accounts[name].checkBalance();
            }
            else
            {
                cout << "Account not found." << endl;
            }
        }
        else if (option == 5)
        {
            cout << "Thank you for using the Banking System. Goodbye!" << endl;
            break;
        }
        else
        {
            cout << "Invalid option. Please try again." << endl;
        }
    }

    return 0;
}
