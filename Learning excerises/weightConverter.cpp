#include <iostream>

using namespace std;

// Function to convert from stone to kilograms
double stoneToKg(double stone)
{
    return stone * 6.35029; // 1 stone = 6.35029 kilograms
}

// Function to convert from kilograms to stone
double kgToStone(double kg)
{
    return kg / 6.35029; // 1 kilogram = 0.157473 stone
}

// Function to convert from kilograms to liters (assuming water density)
double kgToLiters(double kg)
{
    return kg; // 1 kilogram of water = 1 liter
}

int main()
{
    int choice;
    double input;

    cout << "Choose conversion option:" << endl;
    cout << "1. Stone to Kilograms" << endl;
    cout << "2. Kilograms to Stone" << endl;
    cout << "3. Kilograms to Liters" << endl;
    cout << "Enter your choice: ";
    cin >> choice;

    switch (choice)
    {
    case 1:
        cout << "Enter weight in stone: ";
        cin >> input;
        cout << input << " stone is equal to " << stoneToKg(input) << " kilograms" << endl;
        break;
    case 2:
        cout << "Enter weight in kilograms: ";
        cin >> input;
        cout << input << " kilograms is equal to " << kgToStone(input) << " stone" << endl;
        break;
    case 3:
        cout << "Enter weight in kilograms: ";
        cin >> input;
        cout << input << " kilograms is equal to " << kgToLiters(input) << " liters" << endl;
        break;
    default:
        cout << "Invalid choice" << endl;
        break;
    }

    return 0;
}
