#include <cstdlib>
#include <iostream>

using namespace std;

int main()
{
    cout << "The system will shut down in 30 seconds...\n";

    // Issue the shutdown command
    // The /s parameter shuts down the PC
    // The /t parameter specifies the delay in seconds (30 seconds in this case)
    system("shutdown /s /t 30");

    return 0;
}
