#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

// Function to sanitize input by removing potentially dangerous characters
string sanitizeInput(const string& input) {
    string sanitized;
    
    // Define a list of dangerous characters to remove
    string dangerousChars = "'\";--/*";
    
    // Iterate over each character in the input string
    for (char ch : input) {
        // Check if the character is not in the dangerous characters list
        if (dangerousChars.find(ch) == string::npos) {
            sanitized += ch; // Add safe characters to the sanitized string
        }
    }
    
    return sanitized;
}

int main() {
    string userInput;
    
    // Example user input
    cout << "Enter input: ";
    getline(cin, userInput);
    
    // Sanitize user input
    string cleanInput = sanitizeInput(userInput);
    
    // Display sanitized input
    cout << "Sanitized input: " << cleanInput << endl;
    
    // You can now use `cleanInput` safely in SQL queries or other contexts
    return 0;
}
