#include <iostream>
using namespace std;

class Gun
{
private:
    int bullets;

public:
    Gun(int initialBullets = 0) : bullets(initialBullets) {}

    void shoot()
    {
        if (bullets > 0)
        {
            cout << "Bang! Remaining bullets: " << --bullets << endl;
        }
        else
        {
            cout << "Click! Out of bullets." << endl;
        }
    }

    void reload(int additionalBullets)
    {
        bullets += additionalBullets;
        cout << "Reloaded. Total bullets: " << bullets << endl;
    }
};

int main()
{
    Gun pistol(6); // Load pistol with 6 bullets

    for (int i = 0; i < 8; ++i)
    {
        pistol.shoot();
    }

    pistol.reload(6);

    return 0;
}
