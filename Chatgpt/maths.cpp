#include <iostream>
#include <typeinfo>
#include <type_traits>

using namespace std;
int main(){
    int num1;
    int num2;
    int usroption;

    cout << "Please enter number 1: ";
    cin >> num1;

    cout << "Please enter number 2: ";
    cin >> num2;

    cout << "From the following options select arithmetric operation: \n 1 - Add \n 2 - Substract \n 3 - Multiple \n 4 - Divide \nYour Option: ";
    cin >> usroption;

    while ( usroption < 1 || usroption > 4) {
        cout << "Invalid menu option!!!";

        // Clear the fail state and discard invalid input
        cin.clear();

        cout << "From the following options select arithmetric operation: \n 1 - Add \n 2 - Substract \n 3 - Multiple \n 4 - Divide \nYour Option: ";
        cin >> usroption;
    }

    switch (usroption)
    {
        case 1:
            cout << "Result: " << num1 + num2 << endl;
        case 2:
            cout << "Results: " << num1 - num2 << endl;
            break; 
        case 3:
            cout << "Results: " << num1 * num2 << endl;
            break; 
        case 4:
            if (num2 != 0) {
                cout << "Result: " << static_cast<double>(num1) / num2 << endl;
            } else {
                cout << "Cannot divide by zero." << endl;
            }
            break; 
        default:
            break;
    }
    cout << endl;

    main();

    return 0 ;
}